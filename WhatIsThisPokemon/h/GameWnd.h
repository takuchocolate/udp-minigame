//=======================================================================================
//
//	  ゲーム用ウィンドウ クラス定義
//
//=======================================================================================
#pragma once

#include "Window.h"
#include "Graphics.h"
#include "Input.h"
#include "Sound.h"
#include "GameMain.h"

class CGameWindow : public CWindow
{
private:
	CGraphics*	m_pGraph;			// 描画デバイス クラス
	CInput*		m_pInput;			// 入出力デバイス クラス
	CSound*		m_pSound;			// サウンドデバイス クラス
	CGameMain*	m_pGameMain;		// ゲームメイン

	bool		m_bWindow;			// ウィンドウ / フルスクリーン

	DWORD		m_dwExecLastTime;	// FPS計測用
	DWORD		m_dwFPSLastTime;
	DWORD		m_dwFrameCount;

public:
	bool InitInstance();
	int ExitInstance();
	bool OnIdle(long lCount);
	void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	bool OnEraseBkgnd(HDC hdc);
	bool OnSetCursor(HWND hwnd, UINT nHitTest, UINT uMouseMsg);
};

//=======================================================================================
//	End of File
//=======================================================================================