//=======================================================================================
//
//	  DirectX Graphics9 クラス定義
//
//=======================================================================================
#pragma once

#include <d3d9.h>
#include <d3dx9.h>

//----------------------------------------------------------------
// 定数・マクロ定義
//----------------------------------------------------------------
#define FOVY			30.0f								// 視野角
#define NEAR_CLIP		1.0f								// ニアクリップを行う距離
#define FAR_CLIP		10000.0f							// ファークリップを行う距離

// 頂点フォーマット( 頂点座標[2D] / 反射光 / テクスチャ座標 )
#define FVF_VERTEX2D	(D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define FVF_VERTEX3D	(D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)

//----------------------------------------------------------------
// 構造体定義
//----------------------------------------------------------------
typedef struct _tVertex2D
{
	D3DXVECTOR4	vtx;		// 頂点座標
	D3DCOLOR	col;		// 反射光
	D3DXVECTOR2	tex;		// テクスチャ座標
} VERTEX2D;

typedef struct _tVertex3D
{
	D3DXVECTOR3	vtx;		// 頂点座標
	D3DCOLOR	col;		// 反射光
	D3DXVECTOR2	tex;		// テクスチャ座標
} VERTEX3D;

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CGraphics
{
private:
	LPDIRECT3D9					m_pD3D;				// Direct3D9 オブジェクト
	static LPDIRECT3DDEVICE9	m_pDevice;			// Direct3DDevice9 オブジェクト
	D3DPRESENT_PARAMETERS		m_D3DPP;			// PRESENT PARAMETERS
	LPD3DXFONT					m_pFont;			// D3DXFont オブジェクト

	D3DXMATRIX					m_MatView;			// ビュー マトリックス
	D3DXMATRIX					m_MatProj;			// 射影マトリックス

	HWND						m_hWnd;				// ウィンドウ ハンドル
	int							m_nWidth;			// 表示領域幅
	int							m_nHeight;			// 表示領域高さ

	static DWORD				m_dwZTestWork;		// Zバッファの現状保存用ワーク
	static DWORD				m_dwZFuncWork;		// Zバッファの現状保存用ワーク

public:
	static LPDIRECT3DDEVICE9 GetDevice() {return m_pDevice;}

	static CGraphics* Create(HWND hWnd, int nWidth, int nHeight, bool bWindow);
	void Release();
	bool Begin();
	void End();
	void SwapBuffer();
	void DrawText(int nX, int nY, LPCTSTR pszText);
	
	static void SetDraw2D(void);		// 2D描画準備
	static void SetDraw3D(void);		// 3D描画準備
	static void DrawTexture(LPDIRECT3DTEXTURE9 pTex,VERTEX2D* pVtx);		// テクスチャ描画

private:
	bool Initialize(HWND hWnd, int nWidth, int nHeight, bool bWindow);
	void Finalize();
};


//=======================================================================================
//	End of File
//=======================================================================================