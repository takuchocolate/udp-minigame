//------------------------------------------------------
//プレイヤークラス
//作成者：近藤拓
//作成日：2014/07/09
//------------------------------------------------------
#pragma once
#include <vector>
#include "Pokemon.h"

class CPlayer
{
private:
	std::vector<CPokemon>	m_myPokemons;	//手持ちポケモン
	CPokemon*				m_pAtk;			//攻撃するポケモン

public:
	CPlayer();

	void Init();
	void Update();
	void Draw();
	void Release();
	
	void put();
	void ShowTypeList();
	std::vector<CPokemon>* getPokenmons(){
		return &m_myPokemons;
	}
	inline CPokemon* getAttacker(){
		return m_pAtk;
	}

};
//------------------------------------------------------
//End of File
//------------------------------------------------------