//================================================================
//		File        : CObject2D.h
//		Program     : 3Dオブジェクトベース
//
//		Description : 3Dオブジェクトベースの定義
//
//		History     : 2014/06/11	作成開始
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include "Object.h"
#include "Graphics.h"
#include "System.h"
#include "Texture.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
const D3DMATERIAL9 DEFAULT_MATERIAL = {	// 汎用マテリアル設定
	{1.0f, 1.0f, 1.0f, 1.0f},		// Diffuse
	{0.1f, 0.1f, 0.1f, 0.1f},		// Ambient
	{0.1f, 0.1f, 0.1f, 0.1f},		// Specular
	{0.1f, 0.1f, 0.1f, 0.1f},		// Emissive
	1.0f							// Power
};

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CObject2D : public CObject
{
// ===== メンバ変数
protected:
	LPDIRECT3DTEXTURE9	m_pTex;			// テクスチャデータ
	VERTEX3D			m_vtx[4];		// 頂点情報
	D3DMATERIAL9		m_material;		// マテリアル
	D3DXVECTOR2			m_size;			// オブジェクトサイズ
	D3DXVECTOR2			m_halfSize;		// オブジェクトサイズ(半分)

// ===== メンバ関数
public:
	void Init();		// デフォルト値で初期化
	void Init(D3DXVECTOR2& size);	// 初期化
	void Init(const float width, const float height);	// 初期化
	void Uninit();		// 後始末
	void Update();		// 更新	
	void Draw();		// 描画(アルファ無効)
	void DrawAlpha();	// 描画(アルファ有効)

	static CObject2D* Create(const LPCTSTR pszFName);		// 生成
	void Release();									// 破棄
	
	// ----- ゲッター
	D3DXVECTOR2& GetSize() {return m_size;}					// サイズ取得
	float GetWidth() const {return m_size.x;}				// オブジェクト幅取得
	float GetHeight() const {return m_size.y;}				// オブジェクト高さ取得
	D3DXVECTOR2& GetHalfSize() {return m_halfSize;}			// ハーフサイズ取得
	float GetHalfWidth() const {return m_halfSize.x;}		// ハーフサイズ取得(幅)
	float GetHalfHeight() const {return m_halfSize.y;}		// ハーフサイズ取得(高さ)
	LPDIRECT3DTEXTURE9	GetTex(){return m_pTex;};			// 
	
	CObject2D();			// コンストラクタ
	virtual ~CObject2D();	// デストラクタ

protected:
	virtual bool Initialize(const LPCTSTR pszFName);	// 初期化
	virtual void Finalize();					// 後始末
	void Resize(D3DXVECTOR2& size);						// オブジェクトサイズ変更
	void Resize(const float width, const float height);	// オブジェクトサイズ変更
	void SetColor(D3DCOLOR color);						// 頂点カラー設定
};

// End of File