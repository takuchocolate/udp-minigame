//------------------------------------------------------
//ディーラークラス
//作成者：近藤拓
//作成日：2014/06/18
//------------------------------------------------------
#pragma once
#include <vector>
#include "Pokemon.h"

//クラス定義
class CDealer
{
private:
	std::vector<CPokemon>	m_bank;		//山札
	CPokemon				m_subject;	//お題
	int						m_index;	//山札の何枚目を引くか
	CPokemon*				m_pAtk[4];	//場のポケモン

	static const int MAX_BANK = 60;		//山札の枚数

public:
	CDealer();

	void PickUp();							//お題を引く
	void Comp(CPokemon* pAttacker);			//相性判定
	void Deal(std::vector<CPokemon>* p);	//カードを配る
	bool Judge(char* answer);				//答え合わせ
	inline void SetAttacker(CPokemon* p, int num){	//場のポケモン設定
		m_pAtk[num] = p;
	}

	void Init();
	void Update();
	void Draw();
	void Release();
};

//------------------------------------------------------
//End of File
//------------------------------------------------------
