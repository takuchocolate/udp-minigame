//================================================================
//		File        : Bullet.h
//		Program     : 弾データ
//
//		Description : 弾データの定義
//
//		History     : 2014/06/13	作成開始
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "Object3D.h"
#include "Enemy.h"

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CBullet : public CObject3D
{
// ===== メンバ変数
private:
	bool	m_bSphere;
	bool	m_bHit;

// ===== メンバ関数
public:
	CBullet();		// コンストラクタ
	~CBullet();		// デストラクタ

	void Init();			// 初期化
	void Uninit();			// 後始末
	void Update();			// 更新
	void Update(CEnemy**);	// 更新
	void Draw();			// 不透明部分描画
	void DrawAlpha(void);	// 半透明部分
	static CBullet* Create(CMesh* pMesh);		//　生成

	void Shot(CObject3D* pObj);		// 発射

	// ----- セッター
	void SetSphere(bool bSphere) {m_bSphere = bSphere;}
	void SetHit(bool bHit) {m_bHit = bHit;}

	// ----- ゲッター
	bool GetSphere(void) {return m_bSphere;}
	bool GetHit(void) {return m_bHit;}

private:
	bool Initialize(CMesh* pMesh);	// 初期化
	void Finalize();				// 後始末
};