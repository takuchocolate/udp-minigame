//================================================================
//		File        : GameMain.h
//		Program     : ゲームのメインクラス
//
//		Description : ゲームのメインクラスの定義
//
//		History     : 2014/04/17	作成開始
//						   05/23	ファイル名変更
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//#ifndef __DEBUG
//#define __DEBUG
//#endif

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "Graphics.h"
#include "Sound.h"
#include "Mesh.h"
#include "Scene.h"
#include "Mouse.h"

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CGameMain
{
// ===== メンバ変数
private:
	CGraphics*	m_pGraph;					// グラフィックデバイス
	CSound*		m_pSound;					// サウンドデバイス

	static TCHAR	m_szDebug[];			// デバッグ用文字列
	int				m_FPS;					// フレーム数カウント用

	//-------- ゲーム用オブジェクト
	static CScene*	m_sceneList[];			// 全シーンのリスト(SCENE_IDにて種別)
	static CMesh*	m_pMesh[];				// メッシュデータ
	static bool		m_bEnd;					// ゲーム終了フラグ

	static CMouse	m_Mouse;				// マウス

	static LPDIRECTSOUNDBUFFER8	m_pBGM[];	// BGMリスト
	static LPDIRECTSOUNDBUFFER8	m_pSE[];	// SEリスト

	static CScene*	m_pScene;			// 現在のシーン
	static int		m_curSceneID;		// 現在のシーンID
	
// ===== メンバ関数
public:
	CGameMain();
	virtual ~CGameMain();

	static CGameMain* Create(CGraphics*, CSound*);
	void Release();
	void Render();
	void Update();
	void SetFPS(int nFPS) {m_FPS = nFPS;}
	void AddDebugStr(LPCTSTR psz) {lstrcat(m_szDebug, psz);}

	static void SetDebugText(TCHAR*);	// デバッグ用文字列セット
	static void SetScene(int id);		// シーン切り替え

	static void	GameEnd(void) {m_bEnd = true;}		// ゲーム終了
	bool GetEndFlg(void) {return m_bEnd;}			// ゲーム終了フラグ取得
	static CMouse GetMouse(void){return m_Mouse;};	// マウス情報取得
	
	static void PlayBGM(int id, int loop = 0, int priority = 0);	// BGM再生
	static void PlaySE(int id, int loop = 0, int priority = 0);		// SE再生
	static void StopBGM(int id);									// BGM停止
	static void StopSE(int id);										// SE停止

private:
	bool Initialize(CGraphics*, CSound*);
	void Finalize();
	void Draw();
};

// End of File