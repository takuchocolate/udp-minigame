//================================================================
//		File        : CardManager.h
//		Program     : カードマネージャベース
//
//		Description : カード管理の定義
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include "../h/Mouse.h"
#include "../h/GameMain.h"
#include "../h/Object2D.h"
#include "../h/Card.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
#define DEFAULT_MAX_CARD (60)

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CCardManager
{
// ===== メンバ関数
private:
	int				m_nCnt;
	LPCTSTR*		m_FileName;
	CCard**			m_pCard;
// ===== メンバ関数
public:
	void Init();		// デフォルト値で初期化
	void Uninit();		// 後始末
	void Update();		// 更新
	void Draw();		// 描画
	void DrawAlpha();	// 透明オブジェ描画

	static CCardManager* Create();		// 生成
	static CCardManager* Create(int nCnt, LPCTSTR* FileName);		// 生成
	void Release();									// 破棄
	
	CCardManager();						// コンストラクタ
	CCardManager(int nCrad);			// 引き数付きコンストラクタ
	virtual ~CCardManager();			// デストラクタ
	
	// ゲッター・セッター
	void			setBG(CCard** pCard){m_pCard	= pCard;};
	void			setCnt(int nCnt){m_nCnt	= nCnt;};
	void			setFileName(LPCTSTR* FileName){m_FileName = FileName;};
	CCard** getCard(){return m_pCard;};
	int				getCnt(){return m_nCnt;};
	LPCTSTR*		getFileName(){return m_FileName;};

protected:
	bool Initialize();					// 初期化
	bool Initialize(int nCnt, LPCTSTR* FileName);	// 初期化
	void Finalize();					// 後始末
};

// End of File