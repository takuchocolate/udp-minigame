//================================================================
//		File        : Scene.h
//		Program     : シーンベース
//
//		Description : ゲームシーンのベース部分
//
//		History     : 2013/12/18	作成開始
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "Mesh.h"
#include "Sound.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
enum _eSceneId
{
	SID_TITLE = 0,		// タイトル
	SID_GAME,			// ゲーム本編
	SID_GAMEOVER,		// ゲームオーバー

	MAX_SCENEID,		// シーン数
};

// ----- メッシュ関連
enum _eMeshType
{
	MT_SKY = 0,		// スカイドーム
	MT_PLAYER,		// プレイヤー
	MT_ENEMY,		// 敵キャラ
	MT_BULLET,		// 弾

	MAX_MESHTYPE
};
const LPCTSTR	XFILE_NAME[MAX_MESHTYPE] = {_T("resource/mesh/sky.x"),		// スカイドーム
											_T("resource/mesh/fright.x"),	// プレイヤー
											_T("resource/mesh/Sepecat_Jaguar.x"),	// 敵キャラ
											_T("resource/mesh/gtank.x"),	// 弾
										};

// ----- BGM関連
enum _eBGMID
{
	BGM_TITLE = 0,		// タイトル
	BGM_GAME,			// ゲーム本編
	BGM_GAMEOVER,		// ゲームオーバー

	MAX_BGMID
};
const LPTSTR	BGM_FILENAME[MAX_BGMID]	= {	_T("res/bgm/Title.wav"),		// タイトル
											_T("res/bgm/Game.wav"),			// ゲーム本編
											_T("res/bgm/Gameover.wav"),		// ゲームオーバー
										};

// ----- SE関連
enum _eSEID
{
	SE_SELECT = 0,		// メニュー選択

	MAX_SEID
};
const LPTSTR	SE_FILENAME[MAX_SEID]	= {	_T("res/se/Select.wav"),			// メニュー選択
										};

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CScene
{
// ===== メンバ変数
protected:

// ===== メンバ関数
public:
	virtual void Init() = 0;		// 初期化
	virtual void Uninit() = 0;		// 後始末
	virtual void Update() = 0;		// 更新
	virtual void Draw() = 0;		// 描画
	virtual void Release();			// 解放
			 
	CScene();	 
	~CScene();	 
				 
protected:		 
	//virtual bool Initialize(CMesh** ppMesh, LPDIRECTSOUNDBUFFER8 pBGM) = 0;		// 初期化
	virtual bool Initialize() = 0;		// 初期化
	virtual void Finalize() = 0;		// 後始末
};