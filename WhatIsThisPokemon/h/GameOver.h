//================================================================
//		File        : GameOver.h
//		Program     : ゲームオーバー画面操作
//
//		Description : ゲームオーバー画面の更新や描画を行い、操作する
//
//		History     : 2013/12/18	作成開始
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "Scene.h"
#include "Graphics.h"
#include "Camera.h"
#include "Texture.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
enum _eTexGameOver
{
	TGO_BG = 0,		// 背景テクスチャ
	TGO_START,		// タイトルへメニューテクスチャ
	TGO_END,		// 終了メニューテクスチャ

	MAX_TEX_GAMEOVER
};

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CGameOver : public CScene
{
// ===== メンバ変数
private:
	CCamera* m_pCamera;		// カメラ
	
// ===== メンバ関数
public:
	void	Init(void);			// 初期化
	void	Uninit(void);		// 後始末
	void	Update(void);		// 更新
	void	Draw(void);			// 描画
	static CGameOver* Create();		// 生成

	CGameOver();
	~CGameOver();

private:
	bool	Initialize();		// 初期化
	void	Finalize(void);		// 後始末
};