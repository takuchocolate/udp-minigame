//================================================================
//		File        : Card.h
//		Program     : カードクラス
//
//		Description : カードクラスの定義
//
//		History     : 2014/06/26	作成開始
//
//										Author : Kei Hashimoto
//================================================================

#pragma once


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include "Object.h"
#include "Object2D.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
#define DEFAULT_CARD_WIDTH	(150)
#define DEFAULT_CARD_HEIGHT	(200)

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CCard : public CObject2D
{
// ===== メンバ変数
private:

// ===== メンバ関数
public:
	void Init();		// 初期化
	void Init(D3DXVECTOR2& size);	// 初期化
	void Init(const float width, const float height);	// 初期化
	void Uninit();		// 後始末
	void Update();		// 更新

	static CCard* Create(const LPCTSTR pszFName);	// 生成
	static CCard* Create(const LPCTSTR pszFName, D3DXVECTOR3 vAxis, float fAngle);	// 生成

	CCard();				// コンストラクタ
	virtual ~CCard();		// デストラクタ

protected:
	bool Initialize(const LPCTSTR pszFName);	// 初期化
	bool Initialize(const LPCTSTR pszFName, D3DXVECTOR3 vAxis, float fAngle);	// 初期化
	void Finalize();					// 後始末
};


// End of File