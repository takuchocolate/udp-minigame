//================================================================
//		File        : CObject.h
//		Program     : オブジェクトベース
//
//		Description : オブジェクトベースの定義
//
//		History     : 2014/06/11	作成開始
//						   06/18	メンバ変数追加
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include "System.h"

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CObject
{
// ===== メンバ変数
protected:
	D3DXMATRIX	m_world;		// ワールド変換行列
	bool		m_bExist;		// 存在フラグ

// ===== メンバ関数
public:
	virtual void Init() = 0;		// 初期化
	virtual void Uninit() = 0;		// 後始末
	virtual void Update() = 0;		// 更新
	virtual void Draw() = 0;		// 描画
	virtual void Release();			// 解放

	// ----- ゲッター
	D3DXMATRIX&	 GetMatrix() {return m_world;}		// ワールド変換行列取得
	D3DXVECTOR3& GetPosition() {return D3DXVECTOR3(
		m_world._41, m_world._42, m_world._43);}	// 現在位置取得
	bool GetExist() const {return m_bExist;}		// 存在フラグ取得

	CObject();				// コンストラクタ
	virtual ~CObject();		// デストラクタ

protected:
	virtual bool Initialize();			// 初期化
	virtual void Finalize() = 0;		// 後始末

public:
	virtual void Translation(D3DXVECTOR3& vec);		// 相対移動
	virtual void Translate(D3DXVECTOR3& pos);		// 絶対移動
	virtual void Rotation(D3DXVECTOR3& angle);		// 相対回転
	virtual void RotationX(float angle);			// 相対回転(X軸)
	virtual void RotationY(float angle);			// 相対回転(Y軸)
	virtual void RotationZ(float angle);			// 相対回転(Z軸)
	virtual void RotationQuaternion(const D3DXVECTOR3* pV, float angle);	// 相対クォータニオン回転
	virtual void Rotate(D3DXVECTOR3& angle);		// 絶対回転
	virtual void RotateX(float angle);				// 絶対回転(X軸)
	virtual void RotateY(float angle);				// 絶対回転(Y軸)
	virtual void RotateZ(float angle);				// 絶対回転(Z軸)
	virtual void RotateQuaternion(const D3DXVECTOR3* pV, float angle);		// 絶対クォータニオン回転
	virtual void Scaling(D3DXVECTOR3& scale);		// 相対拡大
	virtual void Scale(D3DXVECTOR3& scale);			// 絶対拡大
};


// End of File