//================================================================
//		File        : BackgroundManager.h
//		Program     : 背景マネージャベース
//
//		Description : 背景管理の定義
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include "../h/Mouse.h"
#include "../h/GameMain.h"
#include "../h/Object2D.h"
#include "../h/Background.h"
#include "../h/ButtonManager.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
#define DEFAULT_MAX_BG	(16)	// 初期背景数
enum _eBG_MNG_STATE
{
	BG_NONE	= 0,
	BG_WAIT,						// 待機
	BG_MOVE_FIELD_TO_POKEMONTABLE,		// フィールドからポケモンテーブルへの推移
	BG_MOVE_POKEMONTABLE_TO_FIELD,	// ポケモンテーブルからフィールドへの推移
	BG_FIELD,
	BG_POKEMONTABLE,

	MAX_BG_STATE
};
//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CBackgroundManager
{
// ===== メンバ関数
private:
	D3DXVECTOR3		m_vPos;
	int				m_nCnt;		
	LPCTSTR*		m_FileName;
	CBackground**	m_pBG;
	CButtonManager*	m_pBtnMng;

	_eBG_MNG_STATE	m_eState;
// ===== メンバ関数
public:
	void Init();		// デフォルト値で初期化
	void Uninit();		// 後始末
	void Update();		// 更新
	void Draw();		// 描画
	void DrawAlpha();	// 透明オブジェ描画
	
	static CBackgroundManager* Create();		// 生成
	static CBackgroundManager* Create(int nCnt, LPCTSTR* FileName);		// 生成
	void Release();									// 破棄

	CBackgroundManager();					// コンストラクタ
	virtual ~CBackgroundManager();			// デストラクタ

	// ゲッター・セッター
	void			setBG(CBackground** pBG){m_pBG	= pBG;};
	void			setCnt(int nCnt){m_nCnt	= nCnt;};
	void			setFileName(LPCTSTR* FileName){m_FileName = FileName;};
	void			setBtnMng(CButtonManager* pBtnMng){m_pBtnMng = pBtnMng;};
	CBackground**	getBG(){return m_pBG;};
	int				getCnt(){return m_nCnt;};
	LPCTSTR*		getFileName(){return m_FileName;};

protected:
	bool Initialize();								// 初期化
	bool Initialize(int nCnt, LPCTSTR* FileName);	// 初期化
	void Finalize();								// 後始末
};

// End of File