//================================================================
//		File        : Mesh.h
//		Program     : メッシュ関連
//
//		Description : メッシュに関する情報の初期化や、操作処理を定義する
//
//		History     : 2013/10/21	作成開始
//					  2014/05/23	ファイル名変更
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <windows.h>		// Windowsプログラムにはこれを付ける
#include <tchar.h>			// 汎用テキスト マッピング
#include <d3d9.h>			// DirectX Graphics 関連のヘッダ
#include <d3dx9.h>			// DirectX Graphics 関連のヘッダ
#include <dxerr.h>			// DirectX Graphics 関連のヘッダ

//----------------------------------------------------------------
// 構造体定義
//----------------------------------------------------------------
struct BVERTEX {
	D3DXVECTOR3	pos;
	D3DXVECTOR3	nor;
	D3DXVECTOR2	tex;
};

struct PARTICLE {
	D3DXVECTOR3 acl;	// 加速度
	D3DXVECTOR3 spd;	// 速度
};

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
#define FVF_BVERTEX		(D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1)

#define INIT_SPD	3.0f	// 初速(要調節)
#define GRAVITY		0.098f	// 重力加速度(要調節)
	
//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CMesh
{
private:
	LPD3DXMESH			m_pD3DMesh;			// D3DXMesh オブジェクト
	DWORD				m_dwNumMaterial;	// マテリアル数
	D3DMATERIAL9*		m_pMaterial;		// マテリアル
	LPDIRECT3DTEXTURE9*	m_ppTexture;		// テクスチャ

	D3DXATTRIBUTERANGE*	m_pAttr;			// 属性テーブル
	DWORD				m_dwAttr;			// 属性数
	DWORD				m_dwVtx;			// 頂点数
	BVERTEX*			m_pVtx;				// 頂点配列
	DWORD				m_dwIdx;			// インデックス数
	WORD*				m_pIdx;				// インデックス配列

	D3DXVECTOR3			m_vHalf;			// 境界ボックス ハーフサイズ
	D3DXVECTOR3			m_vCenter;			// 境界ボックス(境界球)中心座標
	float				m_fRadius;			// 境界球半径
	LPD3DXMESH			m_pSphere;			// 境界球表示用メッシュ

	PARTICLE*			m_pPiece;			// 断片の制御用
	BVERTEX*			m_pPieceVtx;
	DWORD				m_dwFace;			// 面数

public:
	CMesh();								// コンストラクタ
	virtual ~CMesh();						// デストラクタ

	static CMesh* Create(LPCTSTR pszFName);		// 生成
	void Release();							// 破棄
	void Draw(D3DXMATRIX& world);			// メッシュ描画
	void Draw(D3DXMATRIX& world, float fAlpha);
	DWORD GetNumVertex() {return m_dwVtx;}	// 頂点数取得
	BVERTEX* GetVertex() {return m_pVtx;}	// 頂点配列取得
	DWORD GetNumIndex() {return m_dwIdx;}	// インデックス数取得
	WORD* GetIndex() {return m_pIdx;}		// インデックス配列取得
	D3DXVECTOR3& GetHalfSize() {return m_vHalf;}	// ボックスハーフサイズ取得
	D3DXVECTOR3& GetCenter() {return m_vCenter;}	// 境界ボックス/球中心座標取得
	float GetRadius() {return m_fRadius;}	// 境界球半径取得
	void InitParticle();					// 断片の初期化
	void UpdateParticle();					// 断片の移動
	void DrawSphere(D3DXMATRIX& world, D3DCOLORVALUE color);
	void DrawNoAlpha(D3DXMATRIX& world);
	void DrawAlpha(D3DXMATRIX& world);
	bool Intersect(LPD3DXVECTOR3 pPos,			// 始点
					LPD3DXVECTOR3 pDir,			// 向き
					LPD3DXVECTOR3 pCross,		// 交点
					LPD3DXVECTOR3 pNormal);		// 交点の法線

private:
	bool Initialize(LPCTSTR pszFName);		// メッシュ初期化
	void Finalize();						// メッシュ解放
};

// End of File