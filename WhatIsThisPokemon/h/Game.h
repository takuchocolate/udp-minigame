//================================================================
//		File        : Game.h
//		Program     : ゲーム本編操作
//
//		Description : ゲーム本編に関する更新や描画を行い、操作する
//
//		History     : 2013/12/18	作成開始
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "MyWinSock.h"
#include "Scene.h"
#include "Graphics.h"
#include "Camera.h"
#include "Pokemon.h"
#include "Dealer.h"
#include "Player.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
enum _eTexGame
{
	TG_BG = 0,		// 背景テクスチャ

	MAX_TEXGAME
};

//フェーズ定数
enum ePhase
{
	PHASE_MATCH,	//マッチング
	PHASE_START,	//ゲーム開始
	//ループ�@
		PHASE_PUT,		//カード選択
		PHASE_WAIT_PUT,	//他プレイヤー待ち
		PHASE_COMP,		//相性判定をうける
		//ループ�A
			PHASE_ANSWER,	//解答受付
			PHASE_WAIT_ANS,	//他解答待ち
			PHASE_JUDGE,	//答え合わせ
		//�A
		PHASE_OPEN,		//答え公開(正解か時間切れ)
	//�@
	PHASE_RESULT
};

enum eDataType
{
	UNI,
	BROAD,

	MAX_DATATYPE
};

struct MatchData : public WINSOCK_PROTOCOL
{
	eDataType type;		//データの種類
	ePhase	phase;		//フェーズ情報
	int		num;		//自分のプレイヤー番号
};

struct GameData : public WINSOCK_PROTOCOL
{
	eDataType type;		//データの種類
	ePhase	phase;		//フェーズ情報
	CDealer dealer;		//ディーラー情報
//	CPokemon subject;	//お題情報
	CPlayer player[4];	//プレイヤー情報
	bool	plyflg[4];	//プレイヤーの存在フラグ
};

struct ClientData : public WINSOCK_PROTOCOL
{

};

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CGame : public CScene
{
// ===== メンバ変数
private:
	// ----- オブジェクト
	CCamera*	m_pCamera;		// カメラ
	CDealer*	m_pDealer;		//ディーラー
	CPlayer*	m_pPlayer[4];	//プレイヤー
	int			m_nPlayerNum;	//プレイヤー番号

	static ePhase		m_phase;	//フェーズ

// ===== メンバ関数
public:
	void	Init(void);			// 初期化
	void	Uninit(void);		// 後始末
	void	Update(void);		// 更新
	void	Draw(void);			// 描画
	static CGame* Create();		// 生成

	//フェーズ設定
	static inline void setPhase(ePhase phase){
		m_phase = phase;
	}
	//フェーズ取得
	static inline ePhase getPhase(){
		return m_phase;
	}

	CGame();
	~CGame();

private:
	bool	Initialize();		// 初期化
	void	Finalize(void);		// 後始末
	void	Select();
	void	Comp();
	void	Server();	//サーバーモード
	void	Client();	//クライアントモード
};