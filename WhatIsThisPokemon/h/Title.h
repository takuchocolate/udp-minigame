//================================================================
//		File        : Title.h
//		Program     : タイトル画面操作
//
//		Description : タイトル画面の更新や描画を行い、操作する
//
//		History     : 2013/12/18	作成開始
//
//										Author : Kei Hashimoto
//================================================================
#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "Scene.h"
#include "Graphics.h"
#include "Camera.h"
#include "Mesh.h"
#include "Texture.h"
#include "Object2D.h"
#include "Card.h"
#include "CardManager.h"
#include "Background.h"
#include "BackgroundManager.h"
#include "Button.h"
#include "ButtonManager.h"
#include "GameWnd.h"
#include "PokemonCard.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
// テクスチャ
enum _eTEX_TITLE
{
	//TT_BG = 0,			// 背景テクスチャ
	//TT_LOGO,			// ロゴテクスチャ
	//TT_START,			// スタートメニューテクスチャ
	//TT_END,			// 終了メニューテクスチャ

	MAX_TEXTITLE
};
// 2Dオブジェクト
enum _eCARD_TITLE
{
	TCARD_PLAYER1	= 0,	// カード

	MAX_CARDTITLE
};
enum _eBG_TITLE
{
	TITLE_FIELD		= 0,	// フィールド
	TITLE_POKEMONTABLE,
	MAX_BGTITLE
};
enum _eBUTTON_TITLE
{
	PIKEMON_BUTTON		= 0,	// フィールド
	TYPE_BUTTON,
	//RETURN_BUTTON,
	MAX_BUTTONTITLE
};

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CTitle : public CScene
{
// ===== メンバ変数
private:
	CCamera*			m_pCamera;					// カメラ
	CBackgroundManager	*m_pBgMng;				// 背景マネージャ
	CCardManager		*m_pCardMng;			// カードマネージャ
	CButtonManager		*m_pBtnMng;				// ボタンマネージャ
	CPokemonCard		*m_PC;

// ===== メンバ関数
public:
	void	Init();				// 初期化
	void	Uninit();			// 後始末
	void	Update();			// 更新
	void	Draw();				// 描画
	static CTitle* Create();	// 生成

	CTitle();
	~CTitle();

private:
	bool	Initialize();		// 初期化
	void	Finalize();			// 後始末
};