//------------------------------------------------------
//ポケモンクラス
//作成者：近藤拓
//作成日：2014/06/18
//------------------------------------------------------
#pragma once
#include <string>
#include <vector>
#include <map>

#define TC  std::string,CPokemon::eComp		//タイプ相性
#define TCT std::string,CPokemon::TypeComp	//タイプ相性表

//特徴
class Feature
{
private:
	std::string m_str;	//特徴のテキスト

public:
	void LoadStr(char* str)	{ if(str)	m_str = str; }
	const char* GetStr()	{ return m_str.c_str();	 }
};

//クラス定義
class CPokemon
{
public:
	enum eComp
	{
		NORMAL,		//普通
		GOOD,		//効果抜群
		BAD,		//効果いまひとつ
		NONE,		//効果なし

		MAX_COMP	//最大数
	};
	typedef std::map<TC> TypeComp;	//タイプ相性型

private:
	std::string	m_name;			//名前
	std::string	m_type;			//タイプ
	Feature		m_features[3];	//特徴
public:
	bool		m_IsFront;		//表裏の状態

	//静的メンバ
	static const int MAX_FEATURE = 3;			//特徴の数
	static std::vector<CPokemon*> PokemonList;	//ポケモン一覧
	static std::map<TCT> TypeCompTable;			//タイプ相性表
	static std::string TypeList[14];			//タイプ一覧

public:
	CPokemon();
	CPokemon(char* name, char* type, char** features);
	void ShowData();	//データ表示
	inline std::string getType(){
		return m_type;
	}
	inline const char* getName(){
		return m_name.c_str();
	}
	static CPokemon* Create(char* data);
	static void LoadPokemonList();
	static void LoadTypeTable();
};


//------------------------------------------------------
//End of File
//------------------------------------------------------