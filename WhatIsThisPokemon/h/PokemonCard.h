//================================================================
//		File        : PokemonCard.h
//		Program     : ポケモンカードベース
//
//		Description : ポケモンカードの定義
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <iostream>
#include <map>
#include <string>

#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include "../h/Mouse.h"
#include "../h/GameMain.h"
#include "../h/Object2D.h"
#include "../h/Card.h"

using namespace std;
//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
#define MAX_POKEMONCARD		(60)
enum _ePOKEMONCARD_TITLE
{
	INSIDE	= 0,	// 表
	OUTSIDE,		// 裏

	MAX_CARD_SIDE
};

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CPokemonCard
{
// ===== メンバ関数
private:
	D3DXVECTOR3 m_vPos;
	D3DXVECTOR3	m_vAxis;
	float		m_fAngle;

	CCard*		m_pCard[MAX_CARD_SIDE];
// ===== メンバ関数
public:
	void Init();		// デフォルト値で初期化
	void Init(const float width, const float height);	// 初期化
	void Uninit();		// 後始末
	void Update();		// 更新
	void Draw();		// 描画
	void DrawAlpha();	// 透明オブジェ描画

	static CPokemonCard* Create();								// 生成
	static CPokemonCard* Create(int nCnt, LPCTSTR* FileName);	// 生成
	static CPokemonCard* Create(std::string);					// 生成
	void Release();												// 破棄
	
	CPokemonCard();						// コンストラクタ
	virtual ~CPokemonCard();			// デストラクタ
	
	// ゲッター・セッター
	//void	setBG(CCard** pCard){m_pCard	= pCard;};
	CCard** getCard(){return m_pCard;};

protected:
	bool Initialize();					// 初期化
	bool Initialize(std::string);					// 初期化
	void Finalize();					// 後始末
};

// End of File