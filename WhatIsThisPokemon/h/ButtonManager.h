//================================================================
//		File        : Button.h
//		Program     : 背景オブジェクトベース
//
//		Description : 背景オブジェクトの定義
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include "../h/Mouse.h"
#include "../h/GameMain.h"
#include "../h/Object2D.h"
#include "../h/Button.h"
#include "../h/BackgroundManager.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
#define	DEFAULT_MAX_BUTTON	(10)
#define BUTTON_SIZE_WIDTH	(150.0f)
#define BUTTON_SIZE_HEIGHT	(30.0f)
#define BUTTON_POS_X		(150.0f/2.0f)
#define BUTTON_POS_Y		(241.0f)

enum _eBUTTON_STATE
{
	NONE = 0,
	WAIT,
	BRIND,

	MAX_STATE
};
//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CButtonManager
{
// ===== メンバ関数
private:
	int			m_nCnt;
	LPCTSTR*	m_FileName;
	CButton**	m_pButton;
	//CBackgroundManager*	m_pBgMng;

// ===== メンバ関数
public:
	void Init();		// デフォルト値で初期化
	void Uninit();		// 後始末
	void Update();		// 更新
	void Draw();		// 描画
	void DrawAlpha();	// 透明オブジェ描画

	static CButtonManager* Create();		// 生成
	static CButtonManager* Create(int nCnt, LPCTSTR* FileName);		// 生成
	void Release();									// 破棄
	
	CButtonManager();						// コンストラクタ
	CButtonManager(int nButton);			// 引き数付きコンストラクタ	
	virtual ~CButtonManager();				// デストラクタ

	void			setBG(CButton** pButton){m_pButton	= pButton;};
	void			setCnt(int nCnt){m_nCnt	= nCnt;};
	void			setFileName(LPCTSTR* FileName){m_FileName = FileName;};
	//void			setBgMng(CBackgroundManager* pBgMng){m_pBgMng = pBgMng;};
	CButton**		getButton(){return m_pButton;};
	int				getCnt(){return m_nCnt;};
	LPCTSTR*		getFileName(){return m_FileName;};

protected:
	bool Initialize();						// 初期化
	bool Initialize(int nCnt, LPCTSTR* FileName);	// 初期化
	void Finalize();						// 後始末
};

// End of File