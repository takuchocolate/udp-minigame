//========================================================================================
//		File        : MyWinSock.h
//		Program     : WinSockクラス
//
//		Description : WinSockクラスの定義
//
//		History     : 2014/07/11	作成開始
//
//																Author : Kei Hashimoto
//========================================================================================

#pragma once

//――――――――――――――――――――――――――――――――――――――――――――
// インクルード
//――――――――――――――――――――――――――――――――――――――――――――
#include <WinSock2.h>
#include "Graphics.h"

#pragma comment(lib, "ws2_32")

//――――――――――――――――――――――――――――――――――――――――――――
// 定数定義
//――――――――――――――――――――――――――――――――――――――――――――
#ifndef __DEBUG
#define __DEBUG
#endif

enum _eCommunicationType	// 通信形態
{
	CT_SERVER = 0,	// サーバ
	CT_CLIENT,		// クライアント

	MAX_COMMUNICATIONTYPE
};

enum _eReceiveStatus	// 受信状態
{
	RS_WAIT = 0,	// 受信待ち
	RS_SUCCESS,		// 成功
	RS_FAIL,		// 失敗

	MAX_RECEIVESTATUS
};

enum _eConnectNum	// 接続番号
{
	CONNECT_1 = 0,	// 接続1
	CONNECT_2,		// 接続2
	CONNECT_3,		// 接続3
	CONNECT_4,		// 接続4

	MAX_CONNECTNUM
};

const int	SERVER_CODE = 151;			// サーバコード
const int	ECHOBUFFER_SIZE = 1024;		// エコーバッファサイズ

//――――――――――――――――――――――――――――――――――――――――――――
// 構造体定義
//――――――――――――――――――――――――――――――――――――――――――――
typedef struct _tWinSockProtocol
{
	int	code;		// サーバコード
	int type;		// 通信形態
} WINSOCK_PROTOCOL;

//――――――――――――――――――――――――――――――――――――――――――――
// クラス定義
//――――――――――――――――――――――――――――――――――――――――――――
class CWinSock
{
// ----- メンバ変数
private:
	static WINSOCK_PROTOCOL		m_protocol;						// プロトコル
	static WSADATA				m_wsaData;						// WinSock ステータス
	static SOCKET				m_sock;							// ソケット
	static struct timeval		m_tv;							// タイムアウト時間
	static struct sockaddr_in	m_bindAddr;						// bind設定用アドレス
	static HANDLE				m_handle;						// スレッド用ハンドル

	// ----- サーバデータ
	static struct sockaddr_in	m_echoServAddr;					// エコーサーバアドレス
	static unsigned short		m_echoServPort;					// エコーサーバポート
	static int					m_recvFlg;						// 受信待ちフラグ(0:受信待ち, 1:受信成功, 2:受信失敗)
	static char*				m_pRecvServData;				// クライアントからの受信データ
	static int					m_recvServDataSize;				// クライアントからの受信データサイズ
	static char*				m_pSendServData[];				// クライアントへの送信データ
	static int					m_sendServDataSize[];			// クライアントへの送信データサイズ
	static struct sockaddr_in	m_connectClntAddr[];			// 通信中クライアントのアドレス
	static char*				m_connectClntData[];			// 通信中クライアントからの受信データ
	static int					m_connect;						// クライアント接続数

	// ----- クライアントデータ
	static struct sockaddr_in	m_echoClntAddr;					// エコークライアントアドレス
	static int					m_cliLen;						// クライアントアドレス長
	static struct sockaddr_in	m_connectServAddr;				// 接続先サーバのアドレス
	static char*				m_pRecvClntData;				// サーバからの受信データ
	static int					m_recvClntDataSize;				// サーバからの受信データサイズ
	static char*				m_pSendClntData;				// サーバへの送信データ
	static int					m_sendClntDataSize;				// サーバへの送信データサイズ

// ----- メンバ関数
public:
	static CWinSock& GetInstance();		// インスタンス取得
	
	static unsigned int WINAPI ReceiveThread(void* arg);	// 受信待ちスレッド

	void	Update();		// 更新処理

	// ----- 共通
	int		GetType() {return m_protocol.type;}		// 通信形態取得

	// ----- サーバ関連
	void				SetSendServerData(int num, WINSOCK_PROTOCOL* pSendServData, int size);				// 送信データ設定
	void				SetSendServerDataAll(WINSOCK_PROTOCOL* pSendServData, int size);					// 送信データ設定(全体)
	WINSOCK_PROTOCOL*	GetRecvServProtocol(int num) {return (WINSOCK_PROTOCOL*)m_connectClntData[num];}	// 受信データ取得
	int					GetRecvFlg() {return m_recvFlg;}													// 受信待ちフラグ取得
	int					GetConnectNum() {return m_connect;}													// 接続数取得

	// ----- クライアント関連
	void				SetSendClientData(WINSOCK_PROTOCOL* pSendClntData, int size);			// 送信データ設定
	WINSOCK_PROTOCOL*	GetRecvClntProtocol() {return (WINSOCK_PROTOCOL*)m_pRecvClntData;}		// 受信データ取得

	// ----- デバッグ関連
#ifdef __DEBUG
	void	DrawMyHost();	// 自分のホスト情報を描画する
	void	DrawAnyHost();	// 任意のホスト情報を描画する
#endif

private:
	CWinSock();
	CWinSock(const CWinSock&) {}
	CWinSock &operator=(const CWinSock&) {}
	~CWinSock();
	
	// ----- 共通
	bool	Init();			// 初期化
	void	Uninit();		// 後始末
	
	// ----- サーバ関連
	bool	StartServer();		// サーバ準備
	void	ExicServer();		// サーバ処理

	// ----- クライアント関連
	bool	StartClient();		// クライアント準備
	void	ExicClient();		// クライアント処理
};


//========================================================================================
//	End of File
//========================================================================================