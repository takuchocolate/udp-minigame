//================================================================
//		File        : Background.h
//		Program     : 背景オブジェクトベース
//
//		Description : 背景オブジェクトの定義
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include "../h/Object2D.h"

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CBackground : public CObject2D
{
// ===== メンバ関数
public:
	void Init();		// デフォルト値で初期化
	void Init(D3DXVECTOR2& size);	// 初期化
	void Init(const float width, const float height);	// 初期化
	void Uninit();		// 後始末
	void Update();		// 更新	

	static CBackground* Create(LPCTSTR pszFName);		// 生成
	
	CBackground();			// コンストラクタ
	virtual ~CBackground();	// デストラクタ

protected:
	bool Initialize(LPCTSTR pszFName);	// 初期化
	void Finalize();					// 後始末
};


// End of File