//================================================================
//		File        : CObject3D.h
//		Program     : 3Dオブジェクトベース
//
//		Description : 3Dオブジェクトベースの定義
//
//		History     : 2014/06/11	作成開始
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include "Object.h"
#include "Mesh.h"
#include "System.h"

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CObject3D : public CObject
{
// ===== メンバ変数
protected:
	CMesh*	m_pMesh;		// メッシュデータ

// ===== メンバ関数
public:
	virtual void Init();		// 初期化
	virtual void Uninit();		// 後始末
	virtual void Update();		// 更新
	virtual void Draw();		// 不透明部分描画
	virtual void DrawAlpha();	// 半透明部分描画
	
	// ----- 当たり判定
	virtual bool Collision(const int id, const CObject3D* pObj);	// 各種当たり判定
	
	bool CollisionSphere(CObject3D* pObj);		// 境界球当たり判定

	// ----- ゲッター
	D3DXVECTOR3	GetMeshCenter() const	{return m_pMesh->GetCenter();}	// 境界球中心座標取得
	float		GetMeshRadius() const	{return m_pMesh->GetRadius();}	// 境界球半径取得
	
	CObject3D();				// コンストラクタ
	virtual ~CObject3D();		// デストラクタ

protected:
	virtual bool Initialize(CMesh* pMesh);	// 初期化
	virtual void Finalize();				// 後始末
};