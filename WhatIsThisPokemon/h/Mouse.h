//================================================================
//		File        : Mouse.h
//		Program     : マウス情報
//
//		Description : マウスの情報を扱う
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>

//----------------------------------------------------------------
// 定数・マクロ定義
//----------------------------------------------------------------
enum _eMOUSE_INPUT
{
	LBUTTON	= 0,
	RBUTTON,

	MAX_BUTTON
};
//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
struct InputMouse
{
	bool m_Read;	// 入力情報
	bool m_Trg;		// 押した瞬間
	bool m_Prs;		// 押してる間
	bool m_Rls;		// 離した瞬間
};
class CMouse
{
// ===== メンバ変数
private:
	POINT		m_MousePos;				// マウス位置
	InputMouse	m_Input[MAX_BUTTON];	// 入力情報
// ===== メンバ関数
public:
	// ----- コンストラクタ
	CMouse();
	~CMouse();
	void Uninit();		// 後始末
	void Update();		// 更新

	POINT	getMousePos(){return m_MousePos;};
	bool	getTrg(int index){return m_Input[index].m_Trg;};
	bool	getPrs(int index){return m_Input[index].m_Prs;};
	bool	getRls(int index){return m_Input[index].m_Rls;};
};


// End of File