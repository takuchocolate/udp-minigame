//================================================================
//		File        : Button.h
//		Program     : 背景オブジェクトベース
//
//		Description : 背景オブジェクトの定義
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include "../h/Mouse.h"
#include "../h/GameMain.h"
#include "../h/Object2D.h"
#include "../h/Input.h"

using namespace Input;

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CButton : public CObject2D
{
// ===== メンバ関数
private:
	bool	m_bPush;	// 押したフラグ
// ===== メンバ関数
public:
	void Init();		// デフォルト値で初期化
	void Init(D3DXVECTOR2& size);	// 初期化
	void Init(const float width, const float height);	// 初期化
	void Uninit();		// 後始末
	static CButton* Create(LPCTSTR pszFName);		
	virtual void Def();	// 通常状態
// 生成
	void Update();		// 更新

	// ボタン処理(オーバーライド)
	virtual void Trg();	// 押した時
	virtual void Prs();	// 押してる
	virtual void Rls();	// 離した時
	
	CButton();			// コンストラクタ
	virtual ~CButton();	// デストラクタ

	bool getPush(){return m_bPush;};
	void setExist(bool bExist){m_bExist = bExist;};

protected:
	bool Initialize(LPCTSTR pszFName);	// 初期化
	void Finalize();					// 後始末
};

// End of File