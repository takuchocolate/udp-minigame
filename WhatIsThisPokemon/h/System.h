//================================================================
//		File        : System.h
//		Program     : ゲーム共通データ/システム
//
//		Description : ゲームの共通データ及びシステムを管理する
//
//		History     : 2013/12/05	作成開始
//					  2014/01/14	フェードイン、フェードアウト関数作成
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include "Graphics.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
// ----- ゲームウィンドウ関連
const int	SCREEN_WIDTH	= 800;			// スクリーンの幅
const int	SCREEN_HEIGHT	= 600;			// スクリーンの高さ
const float	FRAME_RATE		= 1000 / 60;	// フレームレート

//----------------------------------------------------------------
// マクロ定義
//----------------------------------------------------------------
// ----- テクスチャアニメーション用
#define TEX_ANIME_LEFT(num, width)				((1.0f / width) * ((num) % width))
#define TEX_ANIME_RIGHT(num, width)				((1.0f / width) * ((num) % width) + (1.0f / width))				
#define TEX_ANIME_TOP(num, width, height)		((1.0f / height) * ((num) / width))				
#define TEX_ANIME_BOTTOM(num, width, height)	((1.0f / height) * ((num) / width) + (1.0f / height))

// ----- 解放用
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(x)			if(x){ x->Release(); x=NULL; }
#endif
#ifndef SAFE_DELETE
#define SAFE_DELETE(x)			if(x){ delete x; x=NULL; }
#endif
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(x)	if(x){ delete[] x; x=NULL; }
#endif

//----------------------------------------------------------------
// 構造体定義
//----------------------------------------------------------------
typedef struct _tAffine
{
	D3DXVECTOR3	scale;		// 拡縮用比率
	D3DXVECTOR3	angle;		// 回転用角度
	D3DXMATRIX	world;		// ワールド変換用行列
} AFFINE;

//----------------------------------------------------------------
// 名前空間定義
//----------------------------------------------------------------
namespace System
{
	// 当たり判定
	void IntersectScreen(D3DXVECTOR3& Pos);		// スクリーン境界判定
	bool IntersectSphere(	// 境界球の当たり判定
							const D3DXVECTOR3&	vCenter1,		// 1つ目の境界球中心座標
							const float			fRadius1,		// 1つ目の境界球半径1
							const D3DXMATRIX&	Matrix1,		// 1つ目のワールド変換行列
							const D3DXVECTOR3&	vCenter2,		// 2つ目の境界球中心座標2
							const float			fRadius2,		// 2つ目の境界球半径2
							const D3DXMATRIX&	Matrix2);		// 2つ目のワールド変換行列2
	void FadeInColor(VERTEX2D* pOut, int nFade);		// フェードイン
	void FadeOutColor(VERTEX2D* pOut, int nFade);		// フェードアウト
	void FlashColor(VERTEX2D* pOut, int nFlash);		// フラッシュ
	void FadeInAlpha(VERTEX2D* pOut, int nFade);		// 透過フェードイン
	void FadeOutAlpha(VERTEX2D* pOut, int nFade);		// 透過フェードアウト
	void FlashAlpha(VERTEX2D* pOut, int nFlash);		// 透過フラッシュ
}