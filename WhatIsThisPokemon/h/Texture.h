//================================================================
//		File        : Texture.h
//		Program     : テクスチャクラス
//
//		Description : テクスチャクラスの定義
//
//		History     : 2014/06/18	作成開始
//						   06/26	メンバを追加
//
//										Author : Kei Hashimoto
//================================================================

#pragma once

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>
#include "Graphics.h"
#include "System.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
const D3DXVECTOR4	DEFAULT_VTX2D_VTX[4]		=	{	D3DXVECTOR4(               0.0f,                 0.0f, 0.0f, 1.0f),		// デフォルト頂点座標(画面全体)
														D3DXVECTOR4((float)SCREEN_WIDTH,                 0.0f, 0.0f, 1.0f),
														D3DXVECTOR4(               0.0f, (float)SCREEN_HEIGHT, 0.0f, 1.0f),
														D3DXVECTOR4((float)SCREEN_WIDTH, (float)SCREEN_HEIGHT, 0.0f, 1.0f),
													};
const D3DXVECTOR3	DEFAULT_VTX3D_VTX[4]		=	{	D3DXVECTOR3(               0.0f,                 0.0f, 0.0f),		// デフォルト頂点座標(画面全体)
														D3DXVECTOR3((float)SCREEN_WIDTH,                 0.0f, 0.0f),
														D3DXVECTOR3(               0.0f, (float)SCREEN_HEIGHT, 0.0f),
														D3DXVECTOR3((float)SCREEN_WIDTH, (float)SCREEN_HEIGHT, 0.0f),
													};
const D3DCOLOR		DEFAULT_VTX_COL_W			=	D3DCOLOR_RGBA(255, 255, 255, 255);		// デフォルト反射光(白色無透明)
const D3DCOLOR		DEFAULT_VTX_COL_W_ALPHA		=	D3DCOLOR_RGBA(255, 255, 255, 0);		// デフォルト反射光(白色透明)
const D3DCOLOR		DEFAULT_VTX_COL_B			=	D3DCOLOR_RGBA(0, 0, 0, 255);			// デフォルト反射光(黒色無透明)
const D3DCOLOR		DEFAULT_VTX_COL_B_ALPHA		=	D3DCOLOR_RGBA(0, 0, 0, 0);				// デフォルト反射光(黒色透明)
const D3DXVECTOR2	DEFAULT_VTX_TEX[4]			=	{	D3DXVECTOR2(0.0f, 0.0f),			// デフォルトテクスチャ座標(画像全体)
														D3DXVECTOR2(1.0f, 0.0f),
														D3DXVECTOR2(0.0f, 1.0f),
														D3DXVECTOR2(1.0f, 1.0f),
													};

//----------------------------------------------------------------
// クラス定義
//----------------------------------------------------------------
class CTexture
{
// ===== メンバ変数
private:
	LPDIRECT3DTEXTURE9	m_pTex;			// テクスチャデータ
	VERTEX2D			m_vtx[4];		// 頂点情報
	D3DXVECTOR2			m_pos;			// 現在位置(テクスチャの中心座標)
	D3DXVECTOR2			m_size;			// テクスチャサイズ
	D3DXVECTOR2			m_halfSize;		// テクスチャサイズ(半分)

// ===== メンバ関数
public:
	void Init();		// デフォルト値で初期化
	void Init(D3DXVECTOR2& pos, D3DXVECTOR2& size);	// 初期化
	void Init(const float x, const float y, const float width, const float height);	// 初期化
	void Uninit();		// 後始末
	void Draw();		// 描画(アルファ無効)
	void DrawAlpha();	// 描画(アルファ有効)

	static CTexture* Create(LPCTSTR pszFName);		// 生成
	void Release();									// 破棄
	
	void SetPosition(D3DXVECTOR2& pos);					// 現在位置更新
	void SetPosition(const float x, const float y);		// 現在位置更新
	void Resize(D3DXVECTOR2& size);						// テクスチャサイズ変更
	void Resize(const float width, const float height);	// テクスチャサイズ変更
	void SetColor(D3DCOLOR color);						// 頂点カラー設定
	
	// ----- ゲッター
	D3DXVECTOR2& GetPosition() {return m_pos;}				// 現在位置取得(テクスチャの中心座標)
	float GetPosX() const {return m_pos.x;}					// 現在位置取得(X座標)
	float GetPosY() const {return m_pos.y;}					// 現在位置取得(Y座標)
	D3DXVECTOR2& GetSize() {return m_size;}					// サイズ取得
	float GetWidth() const {return m_size.x;}				// テクスチャ幅取得
	float GetHeight() const {return m_size.y;}				// テクスチャ高さ取得
	D3DXVECTOR2& GetHalfSize() {return m_halfSize;}			// ハーフサイズ取得
	float GetHalfWidth() const {return m_halfSize.x;}		// ハーフサイズ取得(幅)
	float GetHalfHeight() const {return m_halfSize.y;}		// ハーフサイズ取得(高さ)
	
	CTexture();				// コンストラクタ
	virtual ~CTexture();	// デストラクタ

private:
	bool Initialize(LPCTSTR pszFName);	// 初期化
	void Finalize();					// 後始末

	void Update();		// 更新
};


// End of File