//================================================================
//		File        : Card.cpp
//		Program     : カードクラス
//
//		Description : カードクラスの実装
//
//		History     : 2014/06/26	作成開始
//
//										Author : Kei Hashimoto
//================================================================

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Card.h"


//================================================================
// public:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CCard::CCard()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CCard::~CCard()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : カードデータを初期化する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CCard::Init(void)
{
		// 頂点データ初期化
	for(int i = 0; i < 4; ++i)
	{
		m_vtx[i].vtx = DEFAULT_VTX3D_VTX[i];	// 画面全体
		m_vtx[i].col = DEFAULT_VTX_COL_W;		// 白色
		m_vtx[i].tex = DEFAULT_VTX_TEX[i];		// UV全体
	}

	m_bExist	= true;
	m_material	= DEFAULT_MATERIAL;		// マテリアル

	Resize(DEFAULT_CARD_WIDTH, DEFAULT_CARD_HEIGHT);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータを初期化する
//	Arguments   : size / オブジェクトサイズ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CCard::Init(D3DXVECTOR2& size)
{
	// 頂点データ初期化
	Init();

	// サイズ設定
	Resize(size);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトを初期化する
//	Arguments   : width  / オブジェクト幅
//				  height / オブジェクト高さ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CCard::Init(const float width, const float height)
{
	Init(D3DXVECTOR2(width, height));
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : カードデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CCard::Uninit(void)
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新
//	Description : カードデータを更新する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CCard::Update(void)
{
	/*
	D3DXVECTOR3 q = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	if(GetAsyncKeyState('W') & 0x8000)
	{
		m_world._41	+= 1.0f;
	}
	if(GetAsyncKeyState('S') & 0x8000)
	{
		m_world._41	-= 1.0f;
	}
	if(GetAsyncKeyState('D') & 0x8000)
	{
		q	= D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		RotationQuaternion(&q, 10);
	}
	*/
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 生成
//	Description : カードオブジェクトを生成する
//	Arguments   : pzsFName / テクスチャファイル名
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CCard* CCard::Create(LPCTSTR pszFName)
{
	// ----- 変数宣言
	CCard* pCard;

	// ----- 初期化処理
	pCard = new CCard();
	if(pCard)
	{
		if(!pCard->Initialize(pszFName))
		{
			SAFE_DELETE(pCard);
		}
	}

	return pCard;
}
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 生成
//	Description : カードオブジェクトを生成する
//	Arguments   : pzsFName / テクスチャファイル名
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CCard* CCard::Create(LPCTSTR pszFName, D3DXVECTOR3 vAxis, float fAngle)
{
	// ----- 変数宣言
	CCard* pCard;

	// ----- 初期化処理
	pCard = new CCard();
	if(pCard)
	{
		if(!pCard->Initialize(pszFName, vAxis, fAngle))
		{
			SAFE_DELETE(pCard);
		}
	}

	return pCard;
}

//================================================================
// private:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : カードデータを初期化する
//	Arguments   : pzsFName / テクスチャファイル名
//	Returns     : 成否
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CCard::Initialize(LPCTSTR pszFName)
{
	// ----- テクスチャ読み込み
	if(FAILED(D3DXCreateTextureFromFile(CGraphics::GetDevice(), pszFName, &m_pTex)))
	{
		::MessageBox(NULL, _T("CCard::Initialize テクスチャの読み込みに失敗しました。"), _T("error"), MB_OK);
		return false;
	}

	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : カードデータを初期化する
//	Arguments   : pzsFName / テクスチャファイル名
//	Returns     : 成否
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CCard::Initialize(LPCTSTR pszFName, D3DXVECTOR3 vAxis, float fAngle)
{
	// ----- テクスチャ読み込み
	if(FAILED(D3DXCreateTextureFromFile(CGraphics::GetDevice(), pszFName, &m_pTex)))
	{
		::MessageBox(NULL, _T("CCard::Initialize テクスチャの読み込みに失敗しました。"), _T("error"), MB_OK);
		return false;
	}

	RotationQuaternion(&vAxis, fAngle);

	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : カードデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CCard::Finalize(void)
{
}


// End of File