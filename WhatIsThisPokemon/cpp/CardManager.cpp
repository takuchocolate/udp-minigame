//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/CardManager.h"

//----------------------------------------------------------------
// グローバル変数宣言
//----------------------------------------------------------------
//----------------------------------------------------------------
// 処理関数
//----------------------------------------------------------------
// デフォルト値で初期化
void CCardManager::Init()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pCard[i]->Init();
	}
}
// 後始末
void CCardManager::Uninit()
{}
// 更新
void CCardManager::Update()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pCard[i]->Update();
	}
}
// 描画
void CCardManager::Draw()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pCard[i]->Draw();
	}
}
// 透明オブジェ描画
void CCardManager::DrawAlpha()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pCard[i]->DrawAlpha();
	}
}
// 生成
CCardManager* CCardManager::Create()
{
	// ----- 変数宣言
	CCardManager* pCardManager;

	// ----- 初期化処理
	pCardManager = new CCardManager();

	pCardManager->setCnt(DEFAULT_MAX_CARD);

	if(pCardManager)
	{
		if(!pCardManager->Initialize())
		{
			SAFE_DELETE(pCardManager);
		}
	}

	return pCardManager;
}
void CCardManager::Release()
{
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pCard[i]->Release();
	}
}
// ----- 背景マネージャ(個数, ファイル名)
CCardManager* CCardManager::Create(int nCnt, LPCTSTR* FileName)
{
	// ----- 変数宣言
	CCardManager* pCardManager;
	// ----- 初期化処理
	pCardManager = new CCardManager();

	if(pCardManager)
	{
		if(!pCardManager->Initialize(nCnt, FileName))
		{
			SAFE_DELETE(pCardManager);
		}
	}
	return pCardManager;
}
// コンストラクタ
CCardManager::CCardManager()
{
	//m_pCard	= new CCard*[m_nCnt];
	m_pCard		= NULL;
	m_FileName	= NULL;
	m_nCnt		= 0;
}
// デストラクタ
CCardManager::~CCardManager()
{}
// 初期化
bool CCardManager::Initialize()
{
	// ------ 背景作成
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pCard[i] = CCard::Create(m_FileName[i]);
	}

	return true;
}
// 初期化
bool CCardManager::Initialize(int nCnt, LPCTSTR* FileName)
{
	// 数を確保
	this->setCnt(nCnt);

	// Card作成
	if(m_pCard != NULL)
		delete[] m_pCard;
	m_pCard	= new CCard*[nCnt];

	// ファイル作成
	if(m_FileName != NULL)
		delete m_FileName;
	m_FileName = new LPCTSTR[sizeof(FileName)/sizeof(LPCTSTR)];
	this->setFileName(FileName);

	// ------ 背景作成
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pCard[i] = CCard::Create(m_FileName[i]);
	}

	return true;
}
// 後始末
void CCardManager::Finalize()
{
	for(int i = 0; i < m_nCnt; ++i)
	{
		SAFE_RELEASE(m_pCard[i]);
	}
}