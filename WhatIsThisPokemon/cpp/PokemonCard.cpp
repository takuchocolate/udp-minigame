//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/PokemonCard.h"

//----------------------------------------------------------------
// グローバル変数宣言
//----------------------------------------------------------------
// ----- リソース情報
typedef std::map<std::string, LPCTSTR> Map;
typedef std::pair<std::string, LPCTSTR> Pair;

Map	PokemonFileNameTable;

LPCTSTR CARD_OUTSIDE_FILENAME =	_T("res/tex/card/outside.png");	// カードテクスチャファイル名
//----------------------------------------------------------------
// 処理関数
//----------------------------------------------------------------
// デフォルト値で初期化
void CPokemonCard::Init()
{
	// ----- 背景
	for(int i = 0; i < MAX_CARD_SIDE; ++i)
	{
		m_pCard[i]->Init();
	}
}
// 引き数で初期化
void CPokemonCard::Init(const float width, const float height)
{
	m_vPos	= D3DXVECTOR3(width, height, 0.0f);
	for(int i = 0; i < MAX_CARD_SIDE; ++i)
	{
		m_pCard[i]->Init();
	}
}
// 後始末
void CPokemonCard::Uninit()
{}
// 更新
void CPokemonCard::Update()
{
	m_pCard[INSIDE]->Translate(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_pCard[OUTSIDE]->Translate(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	// ----- 背景
	for(int i = 0; i < MAX_CARD_SIDE; ++i)
	{
		m_pCard[i]->Update();
	}
	if(GetAsyncKeyState('W') & 0x8000)
	{
		m_vPos.x	+= 1.0f;
	}
	if(GetAsyncKeyState('S') & 0x8000)
	{
		m_vPos.x	-= 1.0f;
	}
	if(GetAsyncKeyState('A') & 0x8000)
	{
		m_fAngle	= -1.0f;
		m_pCard[INSIDE]->RotationQuaternion(&m_vAxis, m_fAngle);
		m_pCard[OUTSIDE]->RotationQuaternion(&m_vAxis, m_fAngle);
	}
	else if(GetAsyncKeyState('D') & 0x8000)
	{
		m_fAngle	= 1.0f;
		m_pCard[INSIDE]->RotationQuaternion(&m_vAxis, m_fAngle);
		m_pCard[OUTSIDE]->RotationQuaternion(&m_vAxis, m_fAngle);
	}
	else
		m_fAngle	= 0.0f;

	m_pCard[INSIDE]->Translate(D3DXVECTOR3(m_vPos.x, m_vPos.y, m_vPos.z + 0.01f));
	m_pCard[OUTSIDE]->Translate(D3DXVECTOR3(m_vPos.x, m_vPos.y, m_vPos.z - 0.01f));
}
// 描画
void CPokemonCard::Draw()
{
	// ----- 背景
	for(int i = 0; i < MAX_CARD_SIDE; ++i)
	{
		m_pCard[i]->Draw();
	}
}
// 透明オブジェ描画
void CPokemonCard::DrawAlpha()
{
	// ----- 背景
	for(int i = 0; i < MAX_CARD_SIDE; ++i)
	{
		m_pCard[i]->DrawAlpha();
	}
}
// 生成
CPokemonCard* CPokemonCard::Create()
{
	// ----- 変数宣言
	CPokemonCard* pPokemonCard;

	// ----- 初期化処理
	pPokemonCard = new CPokemonCard();

	if(pPokemonCard)
	{
		if(!pPokemonCard->Initialize())
		{
			SAFE_DELETE(pPokemonCard);
		}
	}

	return pPokemonCard;
}
void CPokemonCard::Release()
{
	for(int i = 0; i < MAX_CARD_SIDE; ++i)
	{
		m_pCard[i]->Release();
	}
}
// ----- 背景マネージャ(個数, ファイル名)
CPokemonCard* CPokemonCard::Create(std::string str)
{
	// ----- 変数宣言
	CPokemonCard* pPokemonCard;
	// ----- 初期化処理
	pPokemonCard = new CPokemonCard();

	if(pPokemonCard)
	{
		if(!pPokemonCard->Initialize(str))
		{
			SAFE_DELETE(pPokemonCard);
		}
	}
	return pPokemonCard;
}
// コンストラクタ
CPokemonCard::CPokemonCard()
{
	// テーブル設定
	PokemonFileNameTable.insert(Pair("ヒトカゲ",	_T("res/tex/card/pokemon_texture_01.png")));
	PokemonFileNameTable.insert(Pair("ロコン",		_T("res/tex/card/pokemon_texture_02.png")));
	PokemonFileNameTable.insert(Pair("ウィンディ",	_T("res/tex/card/pokemon_texture_03.png")));
	PokemonFileNameTable.insert(Pair("ギャロップ",	_T("res/tex/card/pokemon_texture_04.png")));
	PokemonFileNameTable.insert(Pair("ブースター",	_T("res/tex/card/pokemon_texture_05.png")));
	PokemonFileNameTable.insert(Pair("ファイヤー",	_T("res/tex/card/pokemon_texture_06.png")));
	PokemonFileNameTable.insert(Pair("ゼニガメ"	 ,	_T("res/tex/card/pokemon_texture_07.png")));
	PokemonFileNameTable.insert(Pair("トサキント",	_T("res/tex/card/pokemon_texture_08.png")));
	PokemonFileNameTable.insert(Pair("ヒトデマン",	_T("res/tex/card/pokemon_texture_09.png")));
	PokemonFileNameTable.insert(Pair("コイキング",	_T("res/tex/card/pokemon_texture_10.png")));
	PokemonFileNameTable.insert(Pair("シャワーズ",	_T("res/tex/card/pokemon_texture_11.png")));
	PokemonFileNameTable.insert(Pair("フシギダネ",	_T("res/tex/card/pokemon_texture_12.png")));
	PokemonFileNameTable.insert(Pair("ナゾノクサ",	_T("res/tex/card/pokemon_texture_13.png")));
	PokemonFileNameTable.insert(Pair("ウツボット",	_T("res/tex/card/pokemon_texture_14.png")));
	PokemonFileNameTable.insert(Pair("ナッシー"	 ,	_T("res/tex/card/pokemon_texture_15.png")));
	PokemonFileNameTable.insert(Pair("モンジャラ",	_T("res/tex/card/pokemon_texture_16.png")));
	PokemonFileNameTable.insert(Pair("ピカチュウ",	_T("res/tex/card/pokemon_texture_17.png")));
	PokemonFileNameTable.insert(Pair("コイル"	  ,	_T("res/tex/card/pokemon_texture_18.png")));
	PokemonFileNameTable.insert(Pair("ビリリダマ",	_T("res/tex/card/pokemon_texture_19.png")));
	PokemonFileNameTable.insert(Pair("サンダース",	_T("res/tex/card/pokemon_texture_20.png")));
	PokemonFileNameTable.insert(Pair("サンダー"	 ,	_T("res/tex/card/pokemon_texture_21.png")));
	PokemonFileNameTable.insert(Pair("ピッピ"	  ,	_T("res/tex/card/pokemon_texture_22.png")));
	PokemonFileNameTable.insert(Pair("プリン"	  ,	_T("res/tex/card/pokemon_texture_23.png")));
	PokemonFileNameTable.insert(Pair("ニャース"	 ,	_T("res/tex/card/pokemon_texture_24.png")));
	PokemonFileNameTable.insert(Pair("メタモン"	 ,	_T("res/tex/card/pokemon_texture_25.png")));
	PokemonFileNameTable.insert(Pair("イーブイ"	 ,	_T("res/tex/card/pokemon_texture_26.png")));
	PokemonFileNameTable.insert(Pair("ポリゴン"	 ,	_T("res/tex/card/pokemon_texture_27.png")));
	PokemonFileNameTable.insert(Pair("カビゴン"	 ,	_T("res/tex/card/pokemon_texture_28.png")));
	PokemonFileNameTable.insert(Pair("バタフリー",	_T("res/tex/card/pokemon_texture_29.png")));
	PokemonFileNameTable.insert(Pair("スピアー"	 ,	_T("res/tex/card/pokemon_texture_30.png")));
	PokemonFileNameTable.insert(Pair("ストライク",	_T("res/tex/card/pokemon_texture_31.png")));
	PokemonFileNameTable.insert(Pair("カイロス"	 ,	_T("res/tex/card/pokemon_texture_32.png")));
	PokemonFileNameTable.insert(Pair("ポッポ"	  ,	_T("res/tex/card/pokemon_texture_33.png")));
	PokemonFileNameTable.insert(Pair("オニスズメ",	_T("res/tex/card/pokemon_texture_34.png")));
	PokemonFileNameTable.insert(Pair("カモネギ"	 ,	_T("res/tex/card/pokemon_texture_35.png")));
	PokemonFileNameTable.insert(Pair("ドードリオ",	_T("res/tex/card/pokemon_texture_36.png")));
	PokemonFileNameTable.insert(Pair("プテラ"	  ,	_T("res/tex/card/pokemon_texture_37.png")));
	PokemonFileNameTable.insert(Pair("アーボック",	_T("res/tex/card/pokemon_texture_38.png")));
	PokemonFileNameTable.insert(Pair("ニドラン♀",	_T("res/tex/card/pokemon_texture_39.png")));
	PokemonFileNameTable.insert(Pair("ニドラン♂",	_T("res/tex/card/pokemon_texture_40.png")));
	PokemonFileNameTable.insert(Pair("マタドガス",	_T("res/tex/card/pokemon_texture_41.png")));
	PokemonFileNameTable.insert(Pair("サンド"	  ,	_T("res/tex/card/pokemon_texture_42.png")));
	PokemonFileNameTable.insert(Pair("ディグダ"	 ,	_T("res/tex/card/pokemon_texture_43.png")));
	PokemonFileNameTable.insert(Pair("カラカラ"	 ,	_T("res/tex/card/pokemon_texture_44.png")));
	PokemonFileNameTable.insert(Pair("オコリザル",	_T("res/tex/card/pokemon_texture_45.png")));
	PokemonFileNameTable.insert(Pair("カイリキー",	_T("res/tex/card/pokemon_texture_46.png")));
	PokemonFileNameTable.insert(Pair("サワムラー",	_T("res/tex/card/pokemon_texture_47.png")));
	PokemonFileNameTable.insert(Pair("エビワラー",	_T("res/tex/card/pokemon_texture_48.png")));
	PokemonFileNameTable.insert(Pair("フーディン",	_T("res/tex/card/pokemon_texture_49.png")));
	PokemonFileNameTable.insert(Pair("スリーパー",	_T("res/tex/card/pokemon_texture_50.png")));
	PokemonFileNameTable.insert(Pair("ミュウツー",	_T("res/tex/card/pokemon_texture_51.png")));
	PokemonFileNameTable.insert(Pair("ミュウ"	  ,	_T("res/tex/card/pokemon_texture_52.png")));
	PokemonFileNameTable.insert(Pair("イシツブテ",	_T("res/tex/card/pokemon_texture_53.png")));
	PokemonFileNameTable.insert(Pair("イワーク"	 ,	_T("res/tex/card/pokemon_texture_54.png")));
	PokemonFileNameTable.insert(Pair("オムスター",	_T("res/tex/card/pokemon_texture_55.png")));
	PokemonFileNameTable.insert(Pair("カブトプス",	_T("res/tex/card/pokemon_texture_56.png")));
	PokemonFileNameTable.insert(Pair("ルージュラ",	_T("res/tex/card/pokemon_texture_57.png")));
	PokemonFileNameTable.insert(Pair("ラプラス"	 ,	_T("res/tex/card/pokemon_texture_58.png")));
	PokemonFileNameTable.insert(Pair("フリーザー",	_T("res/tex/card/pokemon_texture_59.png")));
	PokemonFileNameTable.insert(Pair("カイリュー",	_T("res/tex/card/pokemon_texture_60.png")));
	//m_pCard	= new CCard*[m_nCnt];
	for(int i = 0; i < MAX_CARD_SIDE; i++)
		m_pCard[i]		= NULL;

	m_vPos		= D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_vAxis		= D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	m_fAngle	= 0.0f;
}
// デストラクタ
CPokemonCard::~CPokemonCard()
{}
// 初期化
bool CPokemonCard::Initialize()
{
	// ------ 背景作成
	for(int i = 0; i < MAX_CARD_SIDE; ++i)
	{
		m_pCard[i] = CCard::Create(CARD_OUTSIDE_FILENAME);
	}

	return true;
}
// 初期化
bool CPokemonCard::Initialize(std::string str)
{
	// ------ 背景作成
	m_pCard[INSIDE]		= CCard::Create(PokemonFileNameTable[str]);
	m_pCard[OUTSIDE]	= CCard::Create(CARD_OUTSIDE_FILENAME,
										D3DXVECTOR3(0.0f, 1.0f, 0.0f),
										180.0f);
	return true;
}
// 後始末
void CPokemonCard::Finalize()
{
	for(int i = 0; i < MAX_CARD_SIDE; ++i)
	{
		SAFE_RELEASE(m_pCard[i]);
	}
}