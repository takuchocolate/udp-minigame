//================================================================
//		File        : Collision.cpp
//		Program     : 当たり判定名前空間
//
//		Description : 当たり判定名前空間の実装
//
//		History     : 2014/06/27	作成開始
//
//										Author : Kei Hashimoto
//================================================================

//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Collision.h"


//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 当たり判定
//	Description : バウンディングボックス同士の当たり判定
//	Arguments   : pObj1 / 判定対象1
//				  pObj2 / 判定対象2
//	Returns     : 判定結果(true:当たっている)
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool Collision::IntersectBoundingBox(const CObject3D* pObj1, const CObject3D* pObj2)
{
	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 当たり判定
//	Description : バウンディングスフィア同士の当たり判定
//	Arguments   : pObj1 / 判定対象1
//				  pObj2 / 判定対象2
//	Returns     : 判定結果(true:当たっている)
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool Collision::IntersectBoundingSpehre(const CObject3D* pObj1, const CObject3D* pObj2)
{
	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 当たり判定
//	Description : OBB同士の当たり判定
//	Arguments   : world1 / 判定対象1のワールド変換行列
//				  obb1   / 判定対象1のOBB
//				  world2 / 判定対象2のワールド変換行列
//				  obb2   / 判定対象2のOBB
//	Returns     : 判定結果(true:当たっている)
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool Collision::IntersectOBB(D3DXMATRIX& world1, OBB& obb1, D3DXMATRIX& world2, OBB& obb2)
{
	// ----- ローカル座標軸の抽出
	D3DXVECTOR3 localAxis[6];	// 判定対象2つ分のローカル座標軸
	localAxis[0] = D3DXVECTOR3(world1._11, world1._12, world1._13);		// 判定対象1のX軸
	localAxis[1] = D3DXVECTOR3(world1._21, world1._22, world1._23);		// 判定対象1のY軸
	localAxis[2] = D3DXVECTOR3(world1._31, world1._32, world1._33);		// 判定対象1のZ軸
	localAxis[3] = D3DXVECTOR3(world2._11, world2._12, world2._13);		// 判定対象2のX軸
	localAxis[4] = D3DXVECTOR3(world2._21, world2._22, world2._23);		// 判定対象2のY軸
	localAxis[5] = D3DXVECTOR3(world2._31, world2._32, world2._33);		// 判定対象2のZ軸

	// ----- ローカル座標軸に長さを反映
	D3DXVECTOR3 halfAxis[6];	// 判定対象2つ分のローカル座標軸
	halfAxis[0] = localAxis[0] * obb1.half.x;		// 判定対象1のX軸
	halfAxis[1] = localAxis[1] * obb1.half.y;		// 判定対象1のY軸
	halfAxis[2] = localAxis[2] * obb1.half.z;		// 判定対象1のZ軸
	halfAxis[3] = localAxis[3] * obb2.half.x;		// 判定対象2のX軸
	halfAxis[4] = localAxis[4] * obb2.half.y;		// 判定対象2のY軸
	halfAxis[5] = localAxis[5] * obb2.half.z;		// 判定対象2のZ軸

	// ----- 中心座標間のベクトル演算
	D3DXVECTOR3 center1, center2;	// 中心座標バッファ
	D3DXVECTOR3 distance;			// 中心座標間の距離
	D3DXVec3TransformCoord(&center1, &obb1.center, &world1);	// 判定対象1の中心座標演算
	D3DXVec3TransformCoord(&center2, &obb2.center, &world2);	// 判定対象2の中心座標演算
	distance = center2 - center1;	// 中心座標間の距離演算

	return true;
}


// End of File