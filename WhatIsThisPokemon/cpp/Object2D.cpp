//================================================================
//		File        : Object2D.cpp
//		Program     : 2Dオブジェクトベース
//
//		Description : 2Dオブジェクトベースの実装
//
//		History     : 2014/06/11	作成開始
//
//										Author : Kei Hashimoto
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Object2D.h"

//================================================================
// public:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CObject2D::CObject2D()
{
	m_pTex		= NULL;
	memset(m_vtx, 0, sizeof(m_vtx));
	m_size		= D3DXVECTOR2(0.0f, 0.0f);
	m_halfSize	= D3DXVECTOR2(0.0f, 0.0f);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CObject2D::~CObject2D()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータをデフォルト値で初期化する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Init()
{
	// 頂点データ初期化
	for(int i = 0; i < 4; ++i)
	{
		m_vtx[i].vtx = DEFAULT_VTX3D_VTX[i];	// 画面全体
		m_vtx[i].col = DEFAULT_VTX_COL_W;		// 白色
		m_vtx[i].tex = DEFAULT_VTX_TEX[i];		// UV全体
	}

	m_material = DEFAULT_MATERIAL;		// マテリアル

	m_bExist = true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータを初期化する
//	Arguments   : size / オブジェクトサイズ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Init(D3DXVECTOR2& size)
{
	// 頂点データ初期化
	Init();

	// サイズ設定
	Resize(size);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトを初期化する
//	Arguments   : width  / オブジェクト幅
//				  height / オブジェクト高さ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Init(const float width, const float height)
{
	Init(D3DXVECTOR2(width, height));
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : オブジェクトデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Uninit(void)
{
	memset(m_vtx, 0, sizeof(m_vtx));	// 頂点情報クリア
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新
//	Description : オブジェクトを更新する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Update(void)
{
	/*
	D3DXVECTOR3 v = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	Translate(v);
	v.z = 10.0f;
	Translate(v);
	D3DXVECTOR3 q = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	RotationQuaternion(&q, 10);
	*/
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 描画
//	Description : オブジェクトを透過無しで描画する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Draw()
{
	if(m_bExist)
	{
		LPDIRECT3DDEVICE9 pDevice = CGraphics::GetDevice();
		pDevice->SetTransform(D3DTS_WORLD, &m_world);

		// ----- 頂点フォーマットの設定
		pDevice->SetFVF(FVF_VERTEX3D);

		// ----- ライティング無効化
		pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

		// ----- 背景テクスチャの設定及びポリゴンの描画
		pDevice->SetMaterial(&m_material);
		pDevice->SetTexture(0, m_pTex);
		pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, m_vtx, sizeof(VERTEX3D));
		
		// ----- ライティング有効化
		pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 描画
//	Description : オブジェクトを透過有りで描画する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::DrawAlpha()
{
	if(m_bExist)
	{
		LPDIRECT3DDEVICE9 pDevice = CGraphics::GetDevice();
		pDevice->SetTransform(D3DTS_WORLD, &m_world);

		// ----- 頂点フォーマットの設定
		pDevice->SetFVF(FVF_VERTEX3D);

		// ----- ライティング無効化
		pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

		 // ----- アルファ ブレンド有効化
		pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
				
		// ----- 背景テクスチャの設定及びポリゴンの描画
		pDevice->SetMaterial(&m_material);
		pDevice->SetTexture(0, m_pTex);
		pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, m_vtx, sizeof(VERTEX3D));
		
		// ----- アルファ ブレンド無効化
		pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		
		// ----- ライティング有効化
		pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 生成
//	Description : オブジェクトを生成する
//	Arguments   : pszFName / 読み込みファイル名
//	Returns     : オブジェクトデータ
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CObject2D* CObject2D::Create(LPCTSTR pszFName)
{
	// ----- 変数宣言
	CObject2D* pObj;

	// ----- 初期化処理
	pObj = new CObject2D();
	if(pObj)
	{
		if(!pObj->Initialize(pszFName))
		{
			SAFE_DELETE(pObj);
		}
	}

	return pObj;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 解放処理
//	Description : オブジェクトデータを解放する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Release()
{
	Finalize();
	delete this;
}


//================================================================
// private:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータを初期化する
//	Arguments   : pszFName / 読み込みファイル名
//	Returns     : 成否
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CObject2D::Initialize(LPCTSTR pszFName)
{
	// ----- テクスチャ読み込み
	if(FAILED(D3DXCreateTextureFromFile(CGraphics::GetDevice(), pszFName, &m_pTex)))
	{
		::MessageBox(NULL, _T("CObject2D::Initialize テクスチャの読み込みに失敗しました。"), _T("error"), MB_OK);
		return false;
	}
	
	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : オブジェクトデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Finalize(void)
{
	SAFE_RELEASE(m_pTex);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : オブジェクトサイズ変更
//	Description : オブジェクトのサイズを変更する
//	Arguments   : size / オブジェクトサイズ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Resize(D3DXVECTOR2& size)
{
	// サイズ設定
	m_size = size;
	m_halfSize = m_size * 0.5f;

	// オブジェクトに反映
	m_vtx[0].vtx.x =  m_halfSize.x;
	m_vtx[0].vtx.y =  m_halfSize.y;
	m_vtx[1].vtx.x = -m_halfSize.x;
	m_vtx[1].vtx.y =  m_halfSize.y;
	m_vtx[2].vtx.x =  m_halfSize.x;
	m_vtx[2].vtx.y = -m_halfSize.y;
	m_vtx[3].vtx.x = -m_halfSize.x;
	m_vtx[3].vtx.y = -m_halfSize.y;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : オブジェクトサイズ変更
//	Description : オブジェクトのサイズを変更する
//	Arguments   : width  / オブジェクト幅
//				  height / オブジェクト高さ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::Resize(const float width, const float height)
{
	Resize(D3DXVECTOR2(width, height));
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 頂点カラー設定
//	Description : ポリゴンの頂点カラーを設定する
//	Arguments   : color / 頂点カラー
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject2D::SetColor(D3DCOLOR color)
{
	// 頂点データ初期化
	for(int i = 0; i < 4; ++i)
		m_vtx[i].col = color;
}


// End of File