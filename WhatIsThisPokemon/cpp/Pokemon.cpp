//------------------------------------------------------
//ポケモンクラス
//作成者：近藤拓
//作成日：2014/06/18
//------------------------------------------------------
#include "../h/Pokemon.h"
#include <string.h>


//プロトタイプ
int strcmacpy(char* dest, char* src);

//静的メンバー
std::vector<CPokemon*> CPokemon::PokemonList;	//ポケモン一覧
std::map<TCT> CPokemon::TypeCompTable;			//タイプ相性表
std::string CPokemon::TypeList[14];				//タイプ一覧

//------------------------------------------------------------
// @要　約：コンストラクタ
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/18	近藤拓	新規作成
//------------------------------------------------------------
CPokemon::CPokemon()
{
	//メンバ初期化
	m_name = "";
	m_type = "";
	for(int i = 0; i < MAX_FEATURE; i++)
	{
		m_features[i].LoadStr("");
	}
}
//------------------------------------------------------------
// @要　約：コンストラクタ
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/18	近藤拓	新規作成
//------------------------------------------------------------
CPokemon::CPokemon(char* name, char* type, char** features)
{
	//メンバ初期化
	m_name = name;
	m_type = type;
	for(int i = 0; i < MAX_FEATURE; i++)
	{
		m_features[i].LoadStr(features[i]);
	}
}
//------------------------------------------------------------
// @要　約：コンストラクタ
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/18	近藤拓	新規作成
//------------------------------------------------------------
CPokemon* CPokemon::Create(char* data)
{
	//変数宣言
	CPokemon* pNew;
	char* sep = ",";
	char* p = strtok(data, sep);
	char* name;
	char* type;
	char* features[3];

	//データがなかったら空のポケモンを返す
	if(data == NULL) return new CPokemon();

	//データの設定
	if(p) name = p;
	p = strtok(NULL, sep);
	if(p) type = p;
	for(int i = 0; i < MAX_FEATURE; i++)
	{
		p = strtok(NULL, sep);
		if(p) features[i] = p;
	}
	pNew = new CPokemon(name, type, features);

	return pNew;
}
//------------------------------------------------------------
// @要　約：ポケモン一覧読み込み
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/25	近藤拓	新規作成
//------------------------------------------------------------
void CPokemon::LoadPokemonList()
{
	FILE* fp;				//ファイルポインタ
	int	 size;				//ファイルサイズ
	char* p;				//データポインタ
	char name[32];			//名前
	char type[32];			//タイプ
	char feature[3][64];	//特徴
	char* features[3];		//特徴配列

	//ファイル展開
	fp = fopen("../PokemonGame/Data/PokemonInfo.csv","rb");
	if(fp)
	{
		//サイズ取得
		fseek(fp, 0L, SEEK_END);
		size = ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		p = new char[size+1];

		//丸ごと読み込み
		fread(p, size, 1, fp);
		p[size] = '\0';
	}
	fclose(fp);

	//カンマで分割
	int PokCnt = 0;
	for(int i = 0; i < size; )
	{
		i += strcmacpy(name, &p[i]);		//名前
		i += strcmacpy(type, &p[i]);		//タイプ
		for(int k = 0; k < 3; k++)			//特徴
		{
			i += strcmacpy(feature[k], &p[i]);
			features[k] = feature[k];
		}
		PokemonList.push_back(new CPokemon(name, type, features));
		PokCnt++;
	}

	delete[] p;
}

//------------------------------------------------------------
// @要　約：カンマ区切りコピー
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/25	近藤拓	新規作成
//------------------------------------------------------------
int strcmacpy(char* dest, char* src)
{
	int i = 0;
	while(1)
	{
		//カンマが来たら終わり
		if(src[i] == ',' || src[i] == '\n')
		{
			if(src[i] == '\n')
				dest[i-1] = '\0';
			else
				dest[i] = '\0';
			i++;
			break;
		}
		//１文字ずつ取得
		dest[i] = src[i];
		i++;
	}

	return i;
}
//------------------------------------------------------------
// @要　約：データ表示
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/07/02	近藤拓	新規作成
//------------------------------------------------------------
void CPokemon::ShowData()
{
	printf("-----------------データ表示-----------------\n");
	printf("%s\n",m_name.c_str());
	printf("%s\n",m_type.c_str());
	printf("%s\n",m_features[0].GetStr());
	printf("%s\n",m_features[1].GetStr());
	printf("%s\n",m_features[2].GetStr());
}

//------------------------------------------------------------
// @要　約：タイプ相性表読み込み
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/07/02	近藤拓	新規作成
//------------------------------------------------------------
void CPokemon::LoadTypeTable()
{
	FILE* fp;				//ファイルポインタ
	int	 size;				//ファイルサイズ
	char* p;				//データポインタ
	char  type[32];		//タイプ

	//ファイル展開
	fp = fopen("../PokemonGame/Type/タイプ相性.csv","rb");
	if(fp)
	{
		//サイズ取得
		fseek(fp, 0L, SEEK_END);
		size = ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		p = new char[size+1];

		//丸ごと読み込み
		fread(p, size, 1, fp);
		p[size] = '\0';
	}
	fclose(fp);

	//テーブル生成
	int nType = 0;
	for(int i = 0; i < size; )
	{
		if(nType < 14)
		{
			i += strcmacpy(type, &p[i]);
			TypeList[nType] = std::string(type);
			nType++;
		}
		else
		{
			char num[3];	//相性データ読み込み先
			for(int v = 0; v < 14; v++)
			{
				std::map<TC> tc;	//タイプ相性マップ
				for(int h = 0; h < 14; h++)
				{
					i += strcmacpy(num, &p[i]);						//文字として読み込み
					eComp comp = (eComp)(num[0] - '0');				//定数化
					tc.insert(std::pair<TC>(TypeList[h], comp));	//相性へ追加
				}
				//テーブルへ追加
				TypeCompTable.insert(std::pair<TCT>(TypeList[v], tc));
			}
		}
	}

	delete[] p;
}
//------------------------------------------------------
//End of File
//------------------------------------------------------