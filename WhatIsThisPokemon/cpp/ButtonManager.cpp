//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Title.h"
#include "../h/ButtonManager.h"

//----------------------------------------------------------------
// グローバル変数宣言
//----------------------------------------------------------------
//----------------------------------------------------------------
// 処理関数
//----------------------------------------------------------------
// デフォルト値で初期化
void CButtonManager::Init()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pButton[i]->Init(BUTTON_SIZE_WIDTH, BUTTON_SIZE_HEIGHT);
	}
	m_pButton[PIKEMON_BUTTON]->Translate(D3DXVECTOR3(BUTTON_POS_X, BUTTON_POS_Y, 0.0f));
	m_pButton[TYPE_BUTTON]->Translate(D3DXVECTOR3(BUTTON_POS_X + BUTTON_SIZE_WIDTH, BUTTON_POS_Y, 0.0f));
}
// 後始末
void CButtonManager::Uninit()
{}
// 更新
void CButtonManager::Update()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pButton[i]->Update();
	}
	//if(m_pBgMng->)
}
// 描画
void CButtonManager::Draw()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pButton[i]->Draw();
	}
}
// 透明オブジェ描画
void CButtonManager::DrawAlpha()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pButton[i]->DrawAlpha();
	}
}
// 生成
CButtonManager* CButtonManager::Create()
{
	// ----- 変数宣言
	CButtonManager* pButtonManager;

	// ----- 初期化処理
	pButtonManager = new CButtonManager();

	pButtonManager->setCnt(DEFAULT_MAX_BUTTON);

	if(pButtonManager)
	{
		if(!pButtonManager->Initialize())
		{
			SAFE_DELETE(pButtonManager);
		}
	}

	return pButtonManager;
}
void CButtonManager::Release()
{
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pButton[i]->Release();
	}
}
// ----- 背景マネージャ(個数, ファイル名)
CButtonManager* CButtonManager::Create(int nCnt, LPCTSTR* FileName)
{
	// ----- 変数宣言
	CButtonManager* pButtonManager;
	// ----- 初期化処理
	pButtonManager = new CButtonManager();

	if(pButtonManager)
	{
		if(!pButtonManager->Initialize(nCnt, FileName))
		{
			SAFE_DELETE(pButtonManager);
		}
	}
	return pButtonManager;
}
// コンストラクタ
CButtonManager::CButtonManager()
{
	m_pButton	= NULL;
	m_FileName	= NULL;
	m_nCnt		= 0;
	//m_pBgMng	= NULL;
}
// デストラクタ
CButtonManager::~CButtonManager()
{}
// 初期化
bool CButtonManager::Initialize()
{
	// ------ 背景作成
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pButton[i] = CButton::Create(m_FileName[i]);
	}

	return true;
}
// 初期化
bool CButtonManager::Initialize(int nCnt, LPCTSTR* FileName)
{
	// 数を確保
	this->setCnt(nCnt);

	// BG作成
	if(m_pButton != NULL)
		delete[] m_pButton;
	m_pButton	= new CButton*[nCnt];

	// ファイル作成
	if(m_FileName != NULL)
		delete m_FileName;
	m_FileName = new LPCTSTR[sizeof(FileName)/sizeof(LPCTSTR)];
	this->setFileName(FileName);

	// ------ 背景作成
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pButton[i] = CButton::Create(m_FileName[i]);
	}

	return true;
}
// 後始末
void CButtonManager::Finalize()
{
	for(int i = 0; i < m_nCnt; ++i)
	{
		SAFE_RELEASE(m_pButton[i]);
	}
}
