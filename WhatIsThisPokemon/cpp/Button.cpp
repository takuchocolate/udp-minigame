//================================================================
//		File        : Button.cpp
//		Program     : 背景オブジェクトベース
//
//		Description : 背景オブジェクトベースの実装
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Button.h"
//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------

//================================================================
// public:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CButton::CButton()
{
	CObject2D::CObject2D();
	m_bPush	= false;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CButton::~CButton()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータをデフォルト値で初期化する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CButton::Init()
{
	// 頂点データ初期化
	for(int i = 0; i < 4; ++i)
	{
		m_vtx[i].vtx = DEFAULT_VTX3D_VTX[i];	// 画面全体
		m_vtx[i].col = DEFAULT_VTX_COL_W;		// 白色
		m_vtx[i].tex = DEFAULT_VTX_TEX[i];		// UV全体
	}

	m_material = DEFAULT_MATERIAL;				// マテリアル

	m_bExist	= true;

	D3DXVECTOR3 vec(SCREEN_WIDTH/2.0f, SCREEN_HEIGHT/2.0f, 0.0f);
	Translation(vec);
}
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータを初期化する
//	Arguments   : size / オブジェクトサイズ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CButton::Init(D3DXVECTOR2& size)
{
	// 頂点データ初期化
	Init();

	// サイズ設定
	Resize(size);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトを初期化する
//	Arguments   : width  / オブジェクト幅
//				  height / オブジェクト高さ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CButton::Init(const float width, const float height)
{
	Init(D3DXVECTOR2(width, height));
}
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : オブジェクトデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CButton::Uninit(void)
{
	memset(m_vtx, 0, sizeof(m_vtx));	// 頂点情報クリア
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新
//	Description : オブジェクトを更新する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CButton::Update(void)
{
	if(m_bExist)
	{
		CObject2D::Update();

		// 押されたら(こんな感じ)
		if(		(SCREEN_WIDTH - GetCurPos().x		<= (GetPosition().x + m_vtx[0].vtx.x)
				&& SCREEN_WIDTH - GetCurPos().x		>= (GetPosition().x + m_vtx[3].vtx.x))
			&&	(SCREEN_HEIGHT - GetCurPos().y		<= (GetPosition().y + m_vtx[0].vtx.y)
				&& SCREEN_HEIGHT - GetCurPos().y	>= (GetPosition().y + m_vtx[3].vtx.y)))
		{	// ボタンの中が触られている
			if(GetTrgClick(0))
			{	// 押された瞬間
				Trg();
			}
			else if(GetPrsClick(0))
			{	// 押している間
				Prs();
			}
			else if(GetRlsClick(0))
			{	// 離した瞬間
				Rls();
			}
			else
			{	// 何もしていない
				Def();
			}
		}
		else
		{	// 何もしてなかったら
			Def();
		}
	}
}
// 押した時
void CButton::Trg()
{
	m_bPush	= true;
}
// 押してる
void CButton::Prs()
{}
// 離した時
void CButton::Rls()
{}	
// 通常状態
void CButton::Def()
{
	m_bPush	= false;
}
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 生成
//	Description : オブジェクトを生成する
//	Arguments   : pszFName / 読み込みファイル名
//	Returns     : オブジェクトデータ
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CButton* CButton::Create(LPCTSTR pszFName)
{
	// ----- 変数宣言
	CButton* pObj;

	// ----- 初期化処理
	pObj = new CButton();
	if(pObj)
	{
		if(!pObj->Initialize(pszFName))
		{
			SAFE_DELETE(pObj);
		}
	}

	return pObj;
}

//================================================================
// private:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータを初期化する
//	Arguments   : pszFName / 読み込みファイル名
//	Returns     : 成否
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CButton::Initialize(LPCTSTR pszFName)
{
	// ----- テクスチャ読み込み
	if(FAILED(D3DXCreateTextureFromFile(CGraphics::GetDevice(), pszFName, &m_pTex)))
	{
		::MessageBox(NULL, _T("CButton::Initialize テクスチャの読み込みに失敗しました。"), _T("error"), MB_OK);
		return false;
	}
	
	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : オブジェクトデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CButton::Finalize(void)
{
	SAFE_RELEASE(m_pTex);
}

// End of File