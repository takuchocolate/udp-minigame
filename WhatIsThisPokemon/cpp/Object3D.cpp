////================================================================
////		File        : Object3D.cpp
////		Program     : 3Dオブジェクトベース
////
////		Description : 3Dオブジェクトベースの実装
////
////		History     : 2014/06/11	作成開始
////
////										Author : Kei Hashimoto
////================================================================
//
//
////----------------------------------------------------------------
//// インクルード
////----------------------------------------------------------------
//#include "../h/Object3D.h"
//#include "../h/Collision.h"
//
//
////================================================================
//// public:
////================================================================
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : コンストラクタ
////	Arguments   : None.
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//CObject3D::CObject3D()
//{
//	m_pMesh	= NULL;
//}
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : デストラクタ
////	Arguments   : None.
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//CObject3D::~CObject3D()
//{
//}
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : 初期化
////	Description : 3Dオブジェクトデータを初期化する
////	Arguments   : None.
////	Returns     : None.
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//void CObject3D::Init(void)
//{
//}
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : 後始末
////	Description : 3Dオブジェクトデータの後始末をする
////	Arguments   : None.
////	Returns     : None.
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//void CObject3D::Uninit(void)
//{
//}
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : 更新
////	Description : 3Dオブジェクトデータを更新する
////	Arguments   : None.
////	Returns     : None.
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//void CObject3D::Update(void)
//{
//}
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : 不透明部分描画
////	Description : 3Dオブジェクトの不透明部分を描画する
////	Arguments   : None.
////	Returns     : None.
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//void CObject3D::Draw(void)
//{
//	if(!m_pMesh)
//		return;
//
//	// ----- ワールド座標変換
//	CGraphics::GetDevice()->SetTransform(D3DTS_WORLD, &m_affine.world);
//	SetTransform();
//
//	// ----- 描画処理
//	m_pMesh->DrawNoAlpha(m_affine.world);
//}
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : 半透明部分描画
////	Description : 3Dオブジェクトの半透明部分を描画する
////	Arguments   : None.
////	Returns     : None.
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//void CObject3D::DrawAlpha(void)
//{
//	if(!m_pMesh)
//		return;
//
//	// ----- ワールド座標変換
//	CGraphics::GetDevice()->SetTransform(D3DTS_WORLD, &m_affine.world);
//	SetTransform();
//
//	// ----- 描画処理
//	m_pMesh->DrawAlpha(m_affine.world);
//}
//
//// 境界球による当たり判定
//bool CObject3D::CollisionSphere(CObject3D* pObj)
//{
//	if (!m_pMesh) return false;
//	if (!pObj) return false;
//	if (!pObj->m_pMesh) return false;
//	D3DXVECTOR3 p0 = pObj->m_pMesh->GetCenter();
//	D3DXVec3TransformCoord(&p0, &p0, &pObj->GetMatrix());
//	float r = pObj->m_pMesh->GetRadius();
//	D3DXVECTOR3 p1 = m_pMesh->GetCenter();
//	D3DXVec3TransformCoord(&p1, &p1, &m_affine.world);
//	r += m_pMesh->GetRadius();
//	float d = D3DXVec3LengthSq(&(p1 - p0));
//	return (d <= r * r);
//}
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : 当たり判定
////	Description : 当たり判定プロシージャ
////	Arguments   : id   / 当たり判定ID
////				  pObj / 判定対象
////	Returns     : 判定結果(true:当たっている)
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//bool CObject::Collision(const int id, const CObject* pObj)
//{
//	using namespace Collision;
//
//	switch(id)
//	{
//		// バウンディングボックス
//		case CID_BOUNDING_BOX:
//			if(IntersectBoundingBox(this, pObj))
//				return true;
//			break;
//			
//		// バウンディングスフィア
//		case CID_BOUNDING_SPHERE:
//			if(IntersectBoundingSpehre(this, pObj))
//				return true;
//			break;
//			
//		// OBB
//		case CID_OBB:
//			if(IntersectOBB(this, pObj))
//				return true;
//			break;
//
//		default:
//			break;
//	}
//
//	return false;	// 当たっていない
//}
//
////================================================================
//// private:
////================================================================
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : 初期化
////	Description : 3Dオブジェクトデータを初期化する
////	Arguments   : pMesh / メッシュデータ
////	Returns     : 成否
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//bool CObject3D::Initialize(CMesh* pMesh)
//{
//	m_pMesh = pMesh;
//
//	return false;
//}
//
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
////	Name        : 後始末
////	Description : 3Dオブジェクトデータの後始末をする
////	Arguments   : None.
////	Returns     : None.
////━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//void CObject3D::Finalize(void)
//{
//}
//
//
//// End of File