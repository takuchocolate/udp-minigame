//================================================================
//		File        : Object.cpp
//		Program     : オブジェクトベース
//
//		Description : オブジェクトベースの実装
//
//		History     : 2014/06/11	作成開始
//
//										Author : Kei Hashimoto
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Object.h"


//================================================================
// public:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CObject::CObject()
{
	D3DXMatrixIdentity(&m_world);
	m_bExist	= false;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CObject::~CObject()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 解放処理
//	Description : オブジェクトデータを解放する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::Release()
{
	Finalize();
	delete this;
}


//================================================================
// private:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータの初期化を行う
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CObject::Initialize()
{
	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 移動処理
//	Description : オブジェクトを相対位置へ移動する
//	Arguments   : vec / 移動量
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::Translation(D3DXVECTOR3& vec)
{

	// ----- 変数宣言
	D3DXMATRIX matTrans;
	
	// ----- 移動処理
	D3DXMatrixTranslation(&matTrans, vec.x, vec.y, vec.z);
	m_world *= matTrans;


	// ----- 移動処理
/*	m_world._41 += vec.x;
	m_world._42 += vec.y;
	m_world._43 += vec.z;
*/
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 移動処理
//	Description : オブジェクトを絶対位置へ移動する
//	Arguments   : pos / 移動先座標
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::Translate(D3DXVECTOR3& pos)
{
	// ----- 移動処理
	m_world._41 = pos.x;
	m_world._42 = pos.y;
	m_world._43 = pos.z;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを相対的に回転する
//	Arguments   : angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::Rotation(D3DXVECTOR3& angle)
{
	// ----- 回転処理
	RotationX(angle.x);
	RotationY(angle.y);
	RotationZ(angle.z);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを相対的にX軸回転する
//	Arguments   : angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::RotationX(float angle)
{
	// ----- 変数宣言
	D3DXMATRIX matRot;

	// ----- 回転処理
	D3DXVECTOR3 axis(m_world._11, m_world._12, m_world._13);
	D3DXMatrixRotationAxis(&matRot, &axis, D3DXToRadian(angle));
	m_world *= matRot;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを相対的にY軸回転する
//	Arguments   : angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::RotationY(float angle)
{
	// ----- 変数宣言
	D3DXMATRIX matRot;

	// ----- 回転処理
	D3DXVECTOR3 axis(m_world._21, m_world._22, m_world._23);
	D3DXMatrixRotationAxis(&matRot, &axis, D3DXToRadian(angle));
	m_world *= matRot;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを相対的にZ軸回転する
//	Arguments   : angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::RotationZ(float angle)
{
	// ----- 変数宣言
	D3DXMATRIX matRot;

	// ----- 回転処理
	D3DXVECTOR3 axis(m_world._31, m_world._32, m_world._33);
	D3DXMatrixRotationAxis(&matRot, &axis, D3DXToRadian(angle));
	m_world *= matRot;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを相対的にクォータニオンで任意軸回転する
//	Arguments   : pV    / 回転軸の方向ベクトル
//				  angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::RotationQuaternion(const D3DXVECTOR3* pV, float angle)
{
	// ----- 変数宣言
	D3DXMATRIX		matQuat;
	D3DXQUATERNION	quat;

	// ----- 回転処理
	D3DXQuaternionRotationAxis(&quat, pV, D3DXToRadian(angle));
	D3DXMatrixRotationQuaternion(&matQuat, &quat);
	m_world *= matQuat;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを絶対的に回転する
//	Arguments   : angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::Rotate(D3DXVECTOR3& angle)
{
	// ----- 変数宣言
	D3DXMATRIX matRot;

	// ----- 回転処理
	D3DXMatrixRotationYawPitchRoll(&matRot, angle.y, angle.x, angle.z);
	m_world *= matRot;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを絶対的にX軸回転する
//	Arguments   : angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::RotateX(float angle)
{
	// ----- 変数宣言
	D3DXMATRIX matRot;

	// ----- 回転をリセット
	m_world._12 = 0;
	m_world._13 = 0;
	m_world._21 = 0;
	m_world._23 = 0;
	m_world._31 = 0;
	m_world._32 = 0;
	
	// ----- 回転処理
	D3DXMatrixRotationX(&matRot, D3DXToRadian(angle));
	m_world *= matRot;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを絶対的にY軸回転する
//	Arguments   : angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::RotateY(float angle)
{
	// ----- 変数宣言
	D3DXMATRIX matRot;

	// ----- 回転をリセット
	m_world._12 = 0;
	m_world._13 = 0;
	m_world._21 = 0;
	m_world._23 = 0;
	m_world._31 = 0;
	m_world._32 = 0;

	// ----- 回転処理
	D3DXMatrixRotationY(&matRot, D3DXToRadian(angle));
	m_world *= matRot;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを絶対的にZ軸回転する
//	Arguments   : angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::RotateZ(float angle)
{
	// ----- 変数宣言
	D3DXMATRIX matRot;
	
	// ----- 回転をリセット
	m_world._12 = 0;
	m_world._13 = 0;
	m_world._21 = 0;
	m_world._23 = 0;
	m_world._31 = 0;
	m_world._32 = 0;

	// ----- 回転処理
	D3DXMatrixRotationZ(&matRot, D3DXToRadian(angle));
	m_world *= matRot;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 回転処理
//	Description : オブジェクトを絶対的にクォータニオンで任意軸回転する
//	Arguments   : pV    / 回転軸の方向ベクトル
//				  angle / 回転角
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::RotateQuaternion(const D3DXVECTOR3* pV, float angle)
{
	// ----- 変数宣言
	D3DXMATRIX		matQuat;
	D3DXQUATERNION	quat;
	
	// ----- 回転をリセット
	m_world._12 = 0;
	m_world._13 = 0;
	m_world._21 = 0;
	m_world._23 = 0;
	m_world._31 = 0;
	m_world._32 = 0;

	// ----- 回転処理
	D3DXQuaternionRotationAxis(&quat, pV, D3DXToRadian(angle));
	D3DXMatrixRotationQuaternion(&matQuat, &quat);
	m_world *= matQuat;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 拡大処理
//	Description : オブジェクトを相対的に拡大する
//	Arguments   : scale / 拡大率
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::Scaling(D3DXVECTOR3& scale)
{
	// ----- 変数宣言
	D3DXMATRIX matScale;

	// ----- 拡大処理
	D3DXMatrixScaling(&matScale, scale.x, scale.y, scale.z);
	m_world *= matScale;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 拡大処理
//	Description : オブジェクトを絶対的に拡大する
//	Arguments   : scale / 拡大率
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CObject::Scale(D3DXVECTOR3& scale)
{
	// ----- 変数宣言
	D3DXMATRIX matScale;

	// ----- 拡大処理
	D3DXMatrixScaling(&matScale, 1.0f, 1.0f, 1.0f);		// 等倍に戻す
	m_world *= matScale;
	D3DXMatrixScaling(&matScale, scale.x, scale.y, scale.z);
	m_world *= matScale;
}


// End of File