//================================================================
//		File        : Background.cpp
//		Program     : 背景オブジェクトベース
//
//		Description : 背景オブジェクトベースの実装
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Background.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------

//================================================================
// public:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CBackground::CBackground()
{
	CObject2D::CObject2D();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CBackground::~CBackground()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータをデフォルト値で初期化する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CBackground::Init()
{
	// 頂点データ初期化
	for(int i = 0; i < 4; ++i)
	{
		m_vtx[i].vtx = DEFAULT_VTX3D_VTX[i];	// 画面全体
		m_vtx[i].col = DEFAULT_VTX_COL_W;		// 白色
		m_vtx[i].tex = DEFAULT_VTX_TEX[i];		// UV全体
	}

	m_bExist	= true;

	m_material = DEFAULT_MATERIAL;				// マテリアル

	D3DXVECTOR3 vec(SCREEN_WIDTH/2.0f, SCREEN_HEIGHT/2.0f, -2000.0f);
	Translation(vec);
	Resize(SCREEN_WIDTH, SCREEN_HEIGHT);		// サイズ指定
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : オブジェクトデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CBackground::Uninit(void)
{
	memset(m_vtx, 0, sizeof(m_vtx));	// 頂点情報クリア
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新
//	Description : オブジェクトを更新する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CBackground::Update(void)
{
	CObject2D::Update();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 生成
//	Description : オブジェクトを生成する
//	Arguments   : pszFName / 読み込みファイル名
//	Returns     : オブジェクトデータ
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CBackground* CBackground::Create(LPCTSTR pszFName)
{
	// ----- 変数宣言
	CBackground* pObj;

	// ----- 初期化処理
	pObj = new CBackground();
	if(pObj)
	{
		if(!pObj->Initialize(pszFName))
		{
			SAFE_DELETE(pObj);
		}
	}

	return pObj;
}

//================================================================
// private:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : オブジェクトデータを初期化する
//	Arguments   : pszFName / 読み込みファイル名
//	Returns     : 成否
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CBackground::Initialize(LPCTSTR pszFName)
{
	// ----- テクスチャ読み込み
	if(FAILED(D3DXCreateTextureFromFile(CGraphics::GetDevice(), pszFName, &m_pTex)))
	{
		::MessageBox(NULL, _T("CBackground::Initialize テクスチャの読み込みに失敗しました。"), _T("error"), MB_OK);
		return false;
	}
	
	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : オブジェクトデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CBackground::Finalize(void)
{
	SAFE_RELEASE(m_pTex);
}

// End of File