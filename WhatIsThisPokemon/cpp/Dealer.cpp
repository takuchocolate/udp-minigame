//------------------------------------------------------
//ディーラークラス
//作成者：近藤拓
//作成日：2014/06/18
//------------------------------------------------------
#include "../h/MyWinSock.h"
#include "../h/Dealer.h"
#include <time.h>
#include <vector>
#include "../h/Game.h"
#include <string>

//------------------------------------------------------------
// @要　約：コンストラクタ
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/18	近藤拓	新規作成
//------------------------------------------------------------
CDealer::CDealer()
{
	//変数宣言-------------------
	//ワーク
	std::vector<CPokemon*> WkPokemon(
		CPokemon::PokemonList.begin(),
		CPokemon::PokemonList.end());

	//山札生成--------------------
	//前処理
	srand((unsigned int)time(NULL));	//乱数シード設定
	//山札のリサイズ
	m_bank.resize(WkPokemon.size());
	//ランダムに山札に加える
	std::vector<CPokemon>::iterator itr,end;
	itr = m_bank.begin();
	end = m_bank.end();
	for(; itr != end; ++itr)
	{
		int idx = rand()%WkPokemon.size();	//ランダムインデックス
		*itr = *WkPokemon[idx];

		WkPokemon.erase(WkPokemon.begin() + idx);
	}
}

//------------------------------------------------------------
// @要　約：初期化
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/18	近藤拓	新規作成
//------------------------------------------------------------
void CDealer::Init()
{
}

//------------------------------------------------------------
// @要　約：更新
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/18	近藤拓	新規作成
//------------------------------------------------------------
void CDealer::Update()
{
	CWinSock& sock = CWinSock::GetInstance();	//ソケット

	ePhase phase = CGame::getPhase();

	switch(phase)
	{
	//ゲーム開始
	case PHASE_START:
		//なんか始まった演出
		//お題を引く
		//カード配る
		//全員に配ったら次のフェーズへ
		bool IsFull;
		IsFull = true;
		for(int i = 0; i < 4; i++)
		{
			if(!m_pAtk[i])
			{
				IsFull = false;
				break;
			}
		}
		if(IsFull)	
			phase = PHASE_PUT;
		break;
	//ポケモン提示
	case PHASE_PUT:
		for(int i = 0; i < 4; i++)
		{

		}
		break;
	//提示待ち
	case PHASE_WAIT_PUT:
		//他の人を待っていますと表示
		break;
	//相性判定
	case PHASE_COMP:
		//ディーラーが出したポケモンとの相性を発表
		break;
	//解答開始
	case PHASE_ANSWER:
		//早押しで解答できる
		break;
	//解答待ち
	case PHASE_WAIT_ANS:
		//他の人が解答中と表示
		break;
	//正誤判定
	case PHASE_JUDGE:
		printf("JUDGE\n");
		//正解ならPHASE_OPENへ、不正解ならPHASE_ANSWERへ
		bool result;

		//プレイヤーからの解答
		char buf[256];
		scanf("%s",buf);
	
		if(strcmp(buf,m_subject.getName()) == 0)
			result = true;
		else
			result = false;
		phase = (result) ? PHASE_OPEN : PHASE_ANSWER;
		break;
	//答え公開
	case PHASE_OPEN:
		printf("OPEN\n");
		//公開
		//正解したプレイヤーへ渡すor破棄
		//PHASE_PUTへ戻る
		break;
	}
}

//------------------------------------------------------------
// @要　約：描画
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/18	近藤拓	新規作成
//------------------------------------------------------------
void CDealer::Draw()
{
	//デバッグ表示
	int i = 0;
	for(std::vector<CPokemon>::iterator itr = m_bank.begin(); itr != m_bank.end(); ++itr)
	{
		printf("%d体目\n",i+1);
		(*itr).ShowData();
		i++;
	}
}

//------------------------------------------------------------
// @要　約：解放
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/06/18	近藤拓	新規作成
//------------------------------------------------------------
void CDealer::Release()
{
}

//------------------------------------------------------------
// @要　約：お題を引く
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/07/09	近藤拓	新規作成
//------------------------------------------------------------
void CDealer::PickUp()
{
	if(m_bank.size() == 0) 
	{
		printf("山札がありません\n");
		return;
	}

	//一番上から一枚引く
	m_subject = m_bank[0];
	m_bank.erase(m_bank.begin());

	printf("今回のお題\n");
	m_subject.ShowData();


	printf("残り%d匹\n",m_bank.size());
}


//------------------------------------------------------------
// @要　約：カードを配る
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/07/09	近藤拓	新規作成
//------------------------------------------------------------
void CDealer::Deal(std::vector<CPokemon>* p)
{
	if(m_bank.size() == 0) return;

	p->push_back(m_bank[0]);
	m_bank.erase(m_bank.begin());
}

//------------------------------------------------------------
// @要　約：相性比較
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/07/13	近藤拓	新規作成
//------------------------------------------------------------
void CDealer::Comp(CPokemon* pAttacker)
{
	CPokemon::eComp result;

	printf("\nタイプ相性判定\n");
	printf("%s",pAttacker->getType().c_str());
	printf("　→　");
	printf("%s",m_subject.getType().c_str());
	printf("\n");
	printf("結果：");
	result = CPokemon::TypeCompTable[pAttacker->getType()][m_subject.getType()];
	switch(result)
	{
	case CPokemon::BAD:
		printf("効果いまひとつ\n");
		break;
	case CPokemon::GOOD:
		printf("効果抜群\n");
		break;
	case CPokemon::NONE:
		printf("効果なし\n");
		break;
	case CPokemon::NORMAL:
		printf("普通\n");
		break;
	}

}
//------------------------------------------------------------
// @要　約：答え合わせ
// @引　数：答えた名前(char*)
// @戻り値：結果(bool)
// @ノート：なし
// @更新日時2014/07/16	近藤拓	新規作成
//------------------------------------------------------------
bool CDealer::Judge(char* answer)
{
	return (strcmp(answer, m_subject.getName()) == 0);
}

//------------------------------------------------------
//End of File
//------------------------------------------------------
