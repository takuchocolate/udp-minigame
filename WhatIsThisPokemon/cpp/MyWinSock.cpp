//========================================================================================
//		File        : MyWinSock.cpp
//		Program     : WinSockクラス
//
//		Description : WinSockクラスの定義
//
//		History     : 2014/07/11	作成開始
//
//																Author : Kei Hashimoto
//========================================================================================

//――――――――――――――――――――――――――――――――――――――――――――
// インクルード
//――――――――――――――――――――――――――――――――――――――――――――
#include "../h/MyWinSock.h"
#include <windows.h>
#include <tchar.h>
#include <process.h>
#include <stdio.h>
#include <vector>
#include "../h/Graphics.h"

//――――――――――――――――――――――――――――――――――――――――――――
// 定数定義
//――――――――――――――――――――――――――――――――――――――――――――
const unsigned short	ECHOSERV_PORT	= 14038;	// エコーサーバポート番号
const int				TIMEOUT_SECS	= 2000;		// タイムアウト秒数

//――――――――――――――――――――――――――――――――――――――――――――
// マクロ定義
//――――――――――――――――――――――――――――――――――――――――――――
#ifndef SAFE_DELETE
#define SAFE_DELETE(x)	if(x){ delete x; x=NULL; }
#endif

//――――――――――――――――――――――――――――――――――――――――――――
// メンバ実体宣言
//――――――――――――――――――――――――――――――――――――――――――――
WINSOCK_PROTOCOL	CWinSock::m_protocol;							// プロトコル
WSADATA				CWinSock::m_wsaData;							// WinSock ステータス
SOCKET				CWinSock::m_sock;								// ソケット
struct timeval		CWinSock::m_tv;									// タイムアウト時間
struct sockaddr_in	CWinSock::m_bindAddr;							// bind設定用アドレス
HANDLE				CWinSock::m_handle;								// スレッド用ハンドル

// ----- サーバデータ
struct sockaddr_in	CWinSock::m_echoServAddr;						// エコーサーバアドレス
unsigned short		CWinSock::m_echoServPort;						// エコーサーバポート
int					CWinSock::m_recvFlg;							// 通信フラグ
char*				CWinSock::m_pRecvServData;						// クライアントからの受信データ
int					CWinSock::m_recvServDataSize;					// クライアントからの受信データサイズ
char*				CWinSock::m_pSendServData[MAX_CONNECTNUM];		// クライアントへの送信データ
int					CWinSock::m_sendServDataSize[MAX_CONNECTNUM];	// クライアントへの送信データサイズ
struct sockaddr_in	CWinSock::m_connectClntAddr[MAX_CONNECTNUM];	// 通信中クライアントのアドレス
char*				CWinSock::m_connectClntData[MAX_CONNECTNUM];	// 通信中クライアントからの受信データ
int					CWinSock::m_connect;							// クライアント接続数

// ----- クライアントデータ
struct sockaddr_in	CWinSock::m_echoClntAddr;						// エコークライアントアドレス
int					CWinSock::m_cliLen;								// クライアントアドレス長
struct sockaddr_in	CWinSock::m_connectServAddr;					// 接続先サーバのアドレス
char*				CWinSock::m_pRecvClntData;						// サーバからの受信データ
int					CWinSock::m_recvClntDataSize;					// サーバからの受信データサイズ
char*				CWinSock::m_pSendClntData;						// サーバへの送信データ
int					CWinSock::m_sendClntDataSize;					// サーバへの送信データサイズ

//――――――――――――――――――――――――――――――――――――――――――――
// グローバル変数宣言
//――――――――――――――――――――――――――――――――――――――――――――
static CWinSock& winSock = CWinSock::GetInstance();		// CWinSockクラスの実体生成


//========================================================================================
// public:
//========================================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : インスタンス取得
//	Description : WinSockクラスのインスタンスを取得する
//	Arguments   : None.
//	Returns     : WinSockクラス
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CWinSock& CWinSock::GetInstance()
{
	// ----- インスタンス生成
	static CWinSock winSock;

	return winSock;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : スレッド処理
//	Description : 受信待ちスレッドを立てる
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
unsigned int CWinSock::ReceiveThread(void* arg)
{
	// ----- クライアントアドレス長取得
	m_cliLen = sizeof(m_echoClntAddr);

	// ----- データ受信
	int r = recvfrom(m_sock, m_pRecvServData, ECHOBUFFER_SIZE, 0, 
					(struct sockaddr*)&m_echoClntAddr, &m_cliLen);

	// ----- 受信データの保存処理
	if(r >= 0)
	{
		// 接続したクライアントのアドレスを保存
		if(m_connect < MAX_CONNECTNUM)
		{
			bool add = false;	// リスト追加フラグ

			// 接続済みかリストを検索
			for(int i = 0; i < m_connect; ++i)
			{
				// 違うゲームだったら未処理
				if(((WINSOCK_PROTOCOL*)m_pRecvServData)->code != SERVER_CODE)
					continue;

				if(m_connectClntAddr[i].sin_addr.S_un.S_addr == m_echoClntAddr.sin_addr.S_un.S_addr)
				{
					add = false;
					break;
				}

				add = true;
			}

			// リストに追加
			if(add || m_connect <= 0)
			{
				m_connectClntAddr[m_connect] = m_echoClntAddr;
				++m_connect;
			}
		}

		// 受信データ保存
		for(int i = 0; i < m_connect; ++i)
		{
			// 違うゲームだったら未処理
			if(((WINSOCK_PROTOCOL*)m_pRecvServData)->code != SERVER_CODE)
				continue;

			if(m_connectClntAddr[i].sin_addr.S_un.S_addr == m_echoClntAddr.sin_addr.S_un.S_addr)
			{
				m_connectClntData[i] = m_pRecvServData;

				// ----- データ返信
				sendto(m_sock, m_pSendServData[i], ECHOBUFFER_SIZE, 0,
					(struct sockaddr*)&m_echoClntAddr, sizeof(m_echoClntAddr));
				
				break;
			}
		}

		m_recvFlg = RS_SUCCESS;		// 受信成功
	}
	else
		m_recvFlg = RS_FAIL;		// 受信失敗
	
	// ----- エラー表示
#ifdef __DEBUG
	printf("server\n");
	printf("Server recvfrom error_code:%d\n", WSAGetLastError());

	for(int i = 0; i < m_connect; ++i)
	{
		printf("%d - ip  :%s\n", i + 1, inet_ntoa(m_connectClntAddr[i].sin_addr));
//		printf("%d - code:%d\n", i + 1, ((WINSOCK_PROTOCOL*)m_connectClntData[i])->code);
		printf("%d - type:%d\n", i + 1, ((WINSOCK_PROTOCOL*)m_connectClntData[i])->type);
	}
#endif

	_endthreadex(0);	// スレッド終了通知
	return 0;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : WinSock通信の準備
//	Arguments   : None.
//	Returns     : 成否(true:成功)
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CWinSock::Init()
{
	// ----- 起動確認
	static bool wakeup = false;		// 起動済みフラグ
	if(wakeup)
	{
		::MessageBox(NULL, _T("CWinSock::起動済みです。"), _T("error"), MB_OK);
		return false;
	}
	
	// ----- デバッグ用コンソール生成
#ifdef __DEBUG
	if(AllocConsole() == 0)
	{
		::MessageBox(NULL, _T("CWinSock::コンソールの生成に失敗しました。"), _T("error"), MB_OK);
		return false;
	}
	freopen("CONOUT$", "w", stdout);
	freopen("CONIN$", "r", stdin);
#endif

	// WinSock立ち上げ
	if(WSAStartup(MAKEWORD(2, 2), &m_wsaData))
	{	
		::MessageBox(NULL, _T("CWinSock::WSAStartupに失敗しました。"), _T("error"), MB_OK);
		return false;
	}

	// UDPデータグラムソケットの作成
	if((m_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
	{
		::MessageBox(NULL, _T("CWinSock::ソケットの作成に失敗しました。"), _T("error"), MB_OK);
		return false;
	}
	
	// ----- タイムアウト準備
	m_tv.tv_sec = TIMEOUT_SECS;
	m_tv.tv_usec = 0;
	if(setsockopt(m_sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&m_tv, sizeof(m_tv)) != 0)
	{
		::MessageBox(NULL, _T("CWinSock::タイムアウトの準備に失敗しました。"), _T("error"), MB_OK);
		return false;
	}
	
	// ----- ロール設定
	// ブロードキャスト準備
	char	ip[256];	// 送信先IPアドレス
	int		perm = 1;	// 
	setsockopt(m_sock, SOL_SOCKET, SO_BROADCAST, (char*)&perm, sizeof(int));
	strcpy(ip, "255.255.255.255");

	// ゲームデータ設定
	m_protocol.code = SERVER_CODE;
	SetSendServerDataAll(&m_protocol, sizeof(m_protocol));
	
	// 送信先情報取得
	m_echoServPort = ECHOSERV_PORT;		// エコーサーバポート番号設定
	m_echoServAddr.sin_family = AF_INET;
	m_echoServAddr.sin_addr.S_un.S_addr = inet_addr(ip);
	m_echoServAddr.sin_port = htons(m_echoServPort);

	// ブロードキャスト送信
	sendto(m_sock, m_pSendServData[0], ECHOBUFFER_SIZE, 0,
			(struct sockaddr*)&m_echoServAddr, sizeof(m_echoServAddr));
	
	// データ受信
	m_pRecvServData = new char[ECHOBUFFER_SIZE];	// 受信バッファ生成
	int recvNum = 0;								// 受信数
	std::vector<sockaddr_in> clntAddrList(1);		// クライアントアドレスリスト
	std::vector<WINSOCK_PROTOCOL*> recvServList(1);	// 検索サーバリスト
	sockaddr_in	clearAddr;							// クリア用
	WINSOCK_PROTOCOL clearPtcl;						// クリア用

	m_cliLen = sizeof(clntAddrList[0]);
	memset(&clearAddr, 0, sizeof(clearAddr));
	memset(&clearPtcl, 0, sizeof(clearPtcl));

	while(1)
	{
		if(recvfrom(m_sock, m_pRecvServData, ECHOBUFFER_SIZE, 0, 
					(struct sockaddr*)&clntAddrList[recvNum], &m_cliLen) == -1)
			break;

		clntAddrList.push_back(clearAddr);
		recvServList[recvNum] = (WINSOCK_PROTOCOL*)m_pRecvServData;
		recvServList.push_back(&clearPtcl);
		++recvNum;
	}

	// ----- エラー表示
#ifdef __DEBUG
	printf("Init recvfrom error_code:%d\n", WSAGetLastError());
#endif

	//-------------------------- @TODO:エラー処理を追加する必要あり
	if(recvNum == 0)
	{
		// ロール登録
		m_protocol.type = CT_SERVER;	// サーバ
	}
	else
	{
		bool rol = false;								// ロール決定フラグ
		std::vector<sockaddr_in>::iterator calit;		// クライアントアドレスリスト
		std::vector<WINSOCK_PROTOCOL*>::iterator rslit;	// 検索サーバリスト
		for(calit = clntAddrList.begin(), rslit = recvServList.begin(); calit != clntAddrList.end(); ++calit, ++rslit)
		{
			if((*rslit)->code != SERVER_CODE)
				continue;

			// ロール登録
			m_protocol.type = CT_CLIENT;	// クライアント
			rol = true;

			// 接続サーバを登録
			m_connectServAddr = *calit;

			break;
		}
		if(rol == false)
		{
			// ロール登録
			m_protocol.type = CT_SERVER;	// サーバ
		}
	}
	
	// ----- WinSock通信準備
	switch(m_protocol.type)
	{
		// サーバ準備
		case CT_SERVER:
			StartServer();
			break;

		// クライアント準備
		case CT_CLIENT:
			StartClient();
			break;

		default:
			break;
	}

	wakeup = true;	// 起動完了

	return true;	// 正常終了
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : WinSock通信の後始末
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CWinSock::Uninit()
{
	if(closesocket(m_sock))
	{
		::MessageBox(NULL, _T("CWinSock::closesocketに失敗しました。"), _T("error"), MB_OK);
	}
	if(WSACleanup())
	{
		::MessageBox(NULL, _T("CWinSock::WSACleanupに失敗しました。"), _T("error"), MB_OK);
	}

	// ----- コンソール破棄
	if(FreeConsole() == 0)
	{
		::MessageBox(NULL, _T("CWinSock::コンソールの破棄に失敗しました。"), _T("error"), MB_OK);
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新処理
//	Description : WinSock通信の更新
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CWinSock::Update()
{
	switch(m_protocol.type)
	{
		// サーバ処理
		case CT_SERVER:
			ExicServer();
			
#ifdef __DEBUG
			// 各種ステータス表示
			if(m_recvFlg == RS_SUCCESS)
			{
//				DrawMyHost();
//				DrawAnyHost();
			}
#endif
			break;

		// クライアント処理
		case CT_CLIENT:
			ExicClient();
			break;

		default:
			break;
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 送信データセット
//	Description : クライアントへの送信データをセットする
//	Arguments   : num           / 接続番号
//				  pSendServData / 送信データ
//				  size          / 送信データサイズ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CWinSock::SetSendServerData(int num, WINSOCK_PROTOCOL* pSendServData, int size)
{
	m_pSendServData[num]	= (char*)pSendServData;
	m_sendServDataSize[num]	= size;
	m_recvServDataSize		= size;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 送信データセット
//	Description : クライアントへの送信データを全バッファにセットする
//	Arguments   : pSendServData / 送信データ
//				  size          / 送信データサイズ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CWinSock::SetSendServerDataAll(WINSOCK_PROTOCOL* pSendServData, int size)
{
	for(int i = 0; i < MAX_CONNECTNUM; ++i)
	{
		m_pSendServData[i]	= (char*)pSendServData;
		m_sendServDataSize[i]	= size;
	}
	m_recvServDataSize		= size;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 送信データセット
//	Description : サーバへの送信データをセットする
//	Arguments   : pSendClntData / 送信データ
//				  size          / 送信データサイズ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CWinSock::SetSendClientData(WINSOCK_PROTOCOL* pSendClntData, int size)
{
	m_pSendClntData		= (char*)pSendClntData;
	m_sendClntDataSize	= size;
	m_recvClntDataSize	= size;
}

#ifdef __DEBUG
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 自分のホスト情報描画
//	Description : 自分のホスト情報を描画する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CWinSock::DrawMyHost()
{
	// ----- 変数宣言
	HOSTENT*	host;				// ホスト情報
	char		pszHostName[100];	// 自分のホスト名

	// ----- 自分のホスト情報を取得
	if(gethostname(pszHostName, 100))
	{
		printf("自分のホストが見つかりませんでした。\n");
	}
	host = gethostbyname(pszHostName);
	if(host == NULL)
	{
		printf("NULLを参照しています。\n");
	}

	// ----- 描画処理
	// ホスト名セット
	printf("ホスト名 = %s\n", host->h_name);
	// エイリアス名セット
	for(int nCnt = 0; host->h_aliases[nCnt]; ++nCnt)
	{
		printf("エイリアス名 = %s\n", host->h_aliases[nCnt]);
	}
	// ホストアドレスのタイプセット
	printf("ホストアドレスのタイプ = %d\n", host->h_addrtype);
	// アドレス長
	printf("アドレス長 = %d\n", host->h_length);
	// アドレスのリスト
	for(int nCnt = 0; host->h_addr_list[nCnt]; ++nCnt)
	{
		printf("IPアドレス = %s\n", inet_ntoa(*(in_addr*)host->h_addr_list[nCnt]));
	}
	printf("\n");
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 任意のホスト情報描画
//	Description : 入力された任意のホスト情報を描画する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CWinSock::DrawAnyHost()
{
}
#endif


//========================================================================================
// private:
//========================================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CWinSock::CWinSock()
{
	// ----- 共通データ
	memset(&m_protocol, 0, sizeof(m_protocol));					// プロトコル
	memset(&m_wsaData, 0, sizeof(m_wsaData));					// WinSock ステータス
	memset(&m_sock, 0, sizeof(m_sock));							// ソケット
	memset(&m_tv, 0, sizeof(m_tv));								// タイムアウト時間
	memset(&m_bindAddr, 0, sizeof(m_bindAddr));					// bind設定用アドレス
	memset(&m_handle, 0, sizeof(m_handle));						// スレッド用ハンドル

	// ----- サーバデータ
	memset(&m_echoServAddr, 0, sizeof(m_echoServAddr));			// エコーサーバアドレス
	m_echoServPort		= 0;									// エコーサーバポート
	m_recvFlg			= MAX_RECEIVESTATUS;					// 通信フラグ
	m_pRecvServData		= NULL;									// クライアントからの受信データ
	m_recvServDataSize	= 0;									// クライアントからの受信データサイズ
	memset(m_pSendServData, 0, sizeof(m_pSendServData));		// クライアントへの送信データ
	memset(m_sendServDataSize, 0, sizeof(m_sendServDataSize));	// クライアントへの送信データサイズ
	memset(m_connectClntAddr, 0, sizeof(m_connectClntAddr));	// 通信中クライアントのアドレス
	memset(m_connectClntData, 0, sizeof(m_connectClntData));	// 通信中クライアントからの受信データ
	m_connect			= 0;									// クライアント接続数

	// ----- クライアントデータ
	memset(&m_echoClntAddr, 0, sizeof(m_echoClntAddr));			// エコークライアントアドレス
	m_cliLen	= 0;											// クライアントアドレス長
	memset(&m_connectServAddr, 0, sizeof(m_connectServAddr));	// 接続先サーバのアドレス
	m_pRecvClntData		= NULL;									// サーバからの受信データ
	m_recvClntDataSize	= 0;									// サーバからの受信データサイズ
	m_pSendClntData		= NULL;									// サーバへの送信データ
	m_sendClntDataSize	= 0;									// サーバへの送信データサイズ

	// ----- 初期化
	Init();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CWinSock::~CWinSock()
{
	Uninit();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : サーバ準備
//	Description : サーバとして初期化する
//	Arguments   : None.
//	Returns     : 成否(true:成功)
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CWinSock::StartServer()
{
	// ----- ソケット破棄
	if(closesocket(m_sock))
	{
		::MessageBox(NULL, _T("CWinSock::closesocketに失敗しました。"), _T("error"), MB_OK);
	}

	// ----- ソケット再生成
	// UDPデータグラムソケットの作成
	if((m_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
	{
		::MessageBox(NULL, _T("CWinSock::ソケットの作成に失敗しました。"), _T("error"), MB_OK);
		return false;
	}
	
	// ----- タイムアウト準備
	m_tv.tv_sec = TIMEOUT_SECS;
	m_tv.tv_usec = 0;
	if(setsockopt(m_sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&m_tv, sizeof(m_tv)) != 0)
	{
		::MessageBox(NULL, _T("CWinSock::タイムアウトの準備に失敗しました。"), _T("error"), MB_OK);
		return false;
	}

	// ----- 通信準備
	m_recvFlg	= RS_WAIT;
	SetSendServerDataAll(&m_protocol, sizeof(m_protocol));

	// ----- メモリ確保
	for(int i = 0; i < MAX_CONNECTNUM; ++i)
		m_connectClntData[i] = new char[ECHOBUFFER_SIZE];
	
	// ----- エコーサーバ設定
	memset(&m_bindAddr, 0, sizeof(m_bindAddr));
	m_bindAddr.sin_family = AF_INET;
	m_bindAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	m_bindAddr.sin_port = htons(m_echoServPort);

	// ----- バインド処理
	if(bind(m_sock, (struct sockaddr*)&m_bindAddr, sizeof(m_bindAddr)) == -1)
	{
		::MessageBox(NULL, _T("CWinSock::bindに失敗しました。"), _T("error"), MB_OK);
		return false;
	}

	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : サーバ処理
//	Description : サーバ処理を行う
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CWinSock::ExicServer()
{
	// ----- 受信待ちスレッド処理開始
	if(!m_handle)
		m_handle = (HANDLE)_beginthreadex(NULL, 0, ReceiveThread, NULL, 0, NULL);
	else
	{
		if(WaitForSingleObject(m_handle, 0) != WAIT_TIMEOUT)
		{
			CloseHandle(m_handle);
			m_handle = NULL;
		}
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : クライアント準備	
//	Description : クライアントとして初期化する
//	Arguments   : None.
//	Returns     : 成否(true:成功)
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CWinSock::StartClient()
{
	// ----- メモリ解放
	delete[] m_pRecvServData;

	// ----- メモリ確保
	m_pRecvClntData = new char[ECHOBUFFER_SIZE];	// 受信バッファ生成

	// ----- 送信先情報取得
	memset(&m_echoClntAddr, 0, sizeof(m_echoClntAddr));
	m_echoClntAddr.sin_family = AF_INET;
	m_echoClntAddr.sin_addr.S_un.S_addr = m_connectServAddr.sin_addr.S_un.S_addr;
	m_echoClntAddr.sin_port = htons(m_echoServPort);

	// ----- クライアントアドレス長取得
	m_cliLen = sizeof(m_echoClntAddr);

	// ----- 送信データセット
	SetSendClientData(&m_protocol, sizeof(m_protocol));

	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : クライアント処理
//	Description : クライアント処理を行う
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CWinSock::ExicClient()
{
	// ----- データ送信
	sendto(m_sock, m_pSendClntData, ECHOBUFFER_SIZE, 0,
		(struct sockaddr*)&m_connectServAddr, sizeof(m_connectServAddr));

	// ----- データ受信
	int r = recvfrom(m_sock, m_pRecvClntData, ECHOBUFFER_SIZE, 0, 
					(struct sockaddr*)&m_echoClntAddr, &m_cliLen);

#ifdef __DEBUG
	printf("client\n");

	HOSTENT* host;			// ホスト情報
	char pszHostName[100];	// 自分のホスト名

	// ----- 自分のホスト情報を取得
	if(gethostname(pszHostName, 100))
	{
		printf("自分のホストが見つかりませんでした。\n");
	}
	host = gethostbyname(pszHostName);
	if(host == NULL)
	{
		printf("NULLを参照しています。\n");
	}
	// ----- 自分のIPアドレス描画
	for(int nCnt = 0; host->h_addr_list[nCnt]; ++nCnt)
	{
		printf("my ip_addr:%s\n", inet_ntoa(*(in_addr*)host->h_addr_list[nCnt]));
	}
#endif
}


//========================================================================================
//	End of File
//========================================================================================