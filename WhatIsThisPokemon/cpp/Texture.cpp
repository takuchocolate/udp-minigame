//================================================================
//		File        : Texture.cpp
//		Program     : テクスチャクラス
//
//		Description : テクスチャクラスの実装
//
//		History     : 2014/06/18	作成開始
//						   06/26	メンバを追加
//
//										Author : Kei Hashimoto
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Texture.h"


//================================================================
// public:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CTexture::CTexture()
{
	m_pTex		= NULL;
	memset(m_vtx, 0, sizeof(m_vtx));
	m_pos		= D3DXVECTOR2(0.0f, 0.0f);
	m_size		= D3DXVECTOR2(0.0f, 0.0f);
	m_halfSize	= D3DXVECTOR2(0.0f, 0.0f);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CTexture::~CTexture()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : テクスチャデータをデフォルト値で初期化する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Init()
{
	// 頂点データ初期化
	for(int i = 0; i < 4; ++i)
	{
		m_vtx[i].vtx = DEFAULT_VTX2D_VTX[i];	// 画面全体
		m_vtx[i].col = DEFAULT_VTX_COL_W;		// 白色
		m_vtx[i].tex = DEFAULT_VTX_TEX[i];		// UV全体
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : テクスチャデータを初期化する
//	Arguments   : pos  / 出現位置(テクスチャの中心座標)
//				  size / テクスチャサイズ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Init(D3DXVECTOR2& pos, D3DXVECTOR2& size)
{
	// 頂点データ初期化
	Init();

	// 描画位置設定
	SetPosition(pos);

	// サイズ設定
	Resize(size);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : テクスチャデータを初期化する
//	Arguments   : x      / 出現位置(X座標)
//				  y      / 出現位置(Y座標)
//				  width  / テクスチャ幅
//				  height / テクスチャ高さ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Init(const float x, const float y, const float width, const float height)
{
	Init(D3DXVECTOR2(x, y), D3DXVECTOR2(width, height));
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : テクスチャデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Uninit(void)
{
	memset(m_vtx, 0, sizeof(m_vtx));	// 頂点情報クリア
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 描画
//	Description : テクスチャを透過有りで描画する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::DrawAlpha()
{
    LPDIRECT3DDEVICE9 pDevice = CGraphics::GetDevice();

	// ----- 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX2D);
	//pDevice->SetFVF(FVF_VERTEX3D);

	 // ----- アルファ ブレンド有効化
    pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
    pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
    pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			
	// ----- 背景テクスチャの設定及びポリゴンの描画
	pDevice->SetTexture(0, m_pTex);
	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, m_vtx, sizeof(VERTEX2D));
	
    // ----- アルファ ブレンド無効化
    pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 描画
//	Description : テクスチャを透過無しで描画する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Draw()
{
    LPDIRECT3DDEVICE9 pDevice = CGraphics::GetDevice();

	// ----- 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX2D);

	// ----- 背景テクスチャの設定及びポリゴンの描画
	pDevice->SetTexture(0, m_pTex);
	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, m_vtx, sizeof(VERTEX2D));
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 生成
//	Description : テクスチャデータを生成する
//	Arguments   : pszFName / 読み込みファイル名
//	Returns     : テクスチャデータ
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CTexture* CTexture::Create(LPCTSTR pszFName)
{
	// ----- 変数宣言
	CTexture* pTexture;

	// ----- 初期化処理
	pTexture = new CTexture();
	if(pTexture)
	{
		if(!pTexture->Initialize(pszFName))
		{
			SAFE_DELETE(pTexture);
		}
	}

	return pTexture;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 解放処理
//	Description : テクスチャデータを解放する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Release()
{
	Finalize();
	delete this;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 現在位置更新
//	Description : テクスチャの現在位置を更新する
//	Arguments   : pos / 出現位置
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::SetPosition(D3DXVECTOR2& pos)
{
	// 現在位置更新
	m_pos = pos;

	// テクスチャに反映
	Update();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 現在位置更新
//	Description : テクスチャの現在位置を更新する
//	Arguments   : x / 出現位置(X座標)
//				  y / 出現位置(Y座標)
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::SetPosition(const float x, const float y)
{
	SetPosition(D3DXVECTOR2(x, y));
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : テクスチャサイズ変更
//	Description : テクスチャのサイズを変更する
//	Arguments   : size / テクスチャサイズ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Resize(D3DXVECTOR2& size)
{
	// サイズ設定
	m_size = size;
	m_halfSize = m_size * 0.5f;

	// テクスチャに反映
	Update();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : テクスチャサイズ変更
//	Description : テクスチャのサイズを変更する
//	Arguments   : width  / テクスチャ幅
//				  height / テクスチャ高さ
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Resize(const float width, const float height)
{
	Resize(D3DXVECTOR2(width, height));
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 頂点カラー設定
//	Description : テクスチャの頂点カラーを設定する
//	Arguments   : color / 頂点カラー
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::SetColor(D3DCOLOR color)
{
	// 頂点データ初期化
	for(int i = 0; i < 4; ++i)
		m_vtx[i].col = color;
}


//================================================================
// private:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : テクスチャデータを初期化する
//	Arguments   : pszFName / 読み込みファイル名
//	Returns     : 成否
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CTexture::Initialize(LPCTSTR pszFName)
{
	// ----- テクスチャ読み込み
	if(FAILED(D3DXCreateTextureFromFile(CGraphics::GetDevice(), pszFName, &m_pTex)))
	{
		::MessageBox(NULL, _T("CTexture::Initialize テクスチャの読み込みに失敗しました。"), _T("error"), MB_OK);
		return false;
	}

	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : テクスチャデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Finalize(void)
{
	SAFE_RELEASE(m_pTex);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新
//	Description : テクスチャデータを更新する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTexture::Update(void)
{
	m_vtx[0].vtx.x = m_pos.x - m_halfSize.x;
	m_vtx[0].vtx.y = m_pos.y - m_halfSize.y;
	m_vtx[1].vtx.x = m_pos.x + m_halfSize.x;
	m_vtx[1].vtx.y = m_pos.y - m_halfSize.y;
	m_vtx[2].vtx.x = m_pos.x - m_halfSize.x;
	m_vtx[2].vtx.y = m_pos.y + m_halfSize.y;
	m_vtx[3].vtx.x = m_pos.x + m_halfSize.x;
	m_vtx[3].vtx.y = m_pos.y + m_halfSize.y;
}


// End of File