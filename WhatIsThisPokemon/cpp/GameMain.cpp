//=======================================================================================
//
//	  ゲーム クラス
//
//=======================================================================================
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NON_CONFORMING_SWPRINTFS
#include "../h/MyWinSock.h"
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include "../h/GameMain.h"
#include "../h/System.h"
#include "../h/Mesh.h"
#include "../h/Input.h"
#include "../h/Title.h"
#include "../h/Game.h"
#include "../h/GameOver.h"

using namespace Input;


//----------------------------------------------------------------
// 定数・マクロ定義
//----------------------------------------------------------------

//----------------------------------------------------------------
// メンバ実体宣言
//----------------------------------------------------------------
TCHAR	CGameMain::m_szDebug[1024];		// デバッグ用文字列

CScene* CGameMain::m_sceneList[MAX_SCENEID];
CMesh*	CGameMain::m_pMesh[MAX_MESHTYPE];
bool	CGameMain::m_bEnd = false;

LPDIRECTSOUNDBUFFER8 CGameMain::m_pBGM[MAX_BGMID];
LPDIRECTSOUNDBUFFER8 CGameMain::m_pSE[MAX_SEID];

CScene* CGameMain::m_pScene;
int		CGameMain::m_curSceneID;
CMouse  CGameMain::m_Mouse;

//---------------------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------------------
CGameMain::CGameMain()
{
	m_pGraph		= NULL;
	m_pSound		= NULL;
	m_szDebug[0]	= _T('\0');
	m_FPS			= 0;

	for(int nCntMesh = 0; nCntMesh < MAX_MESHTYPE; ++nCntMesh)
		m_pMesh[nCntMesh] = NULL;
	for(int nCntBGM = 0; nCntBGM < MAX_BGMID; ++nCntBGM)
		m_pBGM[nCntBGM] = NULL;
	for(int nCntSE = 0; nCntSE < MAX_SEID; ++nCntSE)
		m_pSE[nCntSE] = NULL;

	m_pScene = NULL;
	m_curSceneID = MAX_SCENEID;
}

//---------------------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------------------
CGameMain::~CGameMain()
{
}

//---------------------------------------------------------------------------------------
// ゲームメイン生成
//---------------------------------------------------------------------------------------
CGameMain* CGameMain::Create(CGraphics* pGraph, CSound* pSound)
{
	CGameMain* pGameMain = new CGameMain();
	if (pGameMain) {
		if (!pGameMain->Initialize(pGraph, pSound)) {
			SAFE_DELETE(pGameMain);
		}
	}
	return pGameMain;
}

//---------------------------------------------------------------------------------------
// デバイス、リソース初期化
//---------------------------------------------------------------------------------------
bool CGameMain::Initialize(CGraphics* pGraph, CSound* pSound)
{
	// ----- システム初期化
	srand(timeBeginPeriod(1));	// 乱数の種を設定

	m_pGraph	= pGraph;
	m_pSound	= pSound;

	// ----- シーン初期化
	m_sceneList[SID_TITLE] = CTitle::Create();
	m_sceneList[SID_GAME] = CGame::Create();
	m_sceneList[SID_GAMEOVER] = CGameOver::Create();

	// 最初のシーンをセット
	SetScene(SID_TITLE);

	// ----- BGM初期化
	for(int nCntBGM = 0; nCntBGM < MAX_BGMID; ++nCntBGM)
		m_pBGM[nCntBGM] = m_pSound->LoadSound(BGM_FILENAME[nCntBGM]);	// BGM読み込み

	// ----- SE初期化
	for(int nCntSE = 0; nCntSE < MAX_SEID; ++nCntSE)
		m_pSE[nCntSE] = m_pSound->LoadSound(SE_FILENAME[nCntSE]);	// SE読み込み

	return true;
}

//---------------------------------------------------------------------------------------
// デバイス、リソース解放
//---------------------------------------------------------------------------------------
void CGameMain::Finalize()
{
	//-------- ゲーム用オブジェクトの後始末
	for(int nCntSE = MAX_SEID - 1; nCntSE >= 0; --nCntSE)
		SAFE_RELEASE(m_pSE[nCntSE]);		// SE解放
	for(int nCntBGM = MAX_BGMID - 1; nCntBGM >= 0; --nCntBGM)
		SAFE_RELEASE(m_pBGM[nCntBGM]);		// BGM解放
	for(int nCntScene = MAX_SCENEID - 1; nCntScene >= 0; --nCntScene)
		SAFE_RELEASE(m_sceneList[nCntScene]);		// シーンテーブル解放
}

//---------------------------------------------------------------------------------------
// シーン解放
//---------------------------------------------------------------------------------------
void CGameMain::Release()
{
	Finalize();
	delete this;
}

//---------------------------------------------------------------------------------------
// ゲーム描画処理（メインループからコールされる）
//---------------------------------------------------------------------------------------
void CGameMain::Render()
{
	if (m_pGraph->Begin()) {	// 描画開始

		Draw();					// 描画処理

		m_pGraph->End();		// 描画終了
	}
	m_pGraph->SwapBuffer();		// バックバッファ入替
}

//---------------------------------------------------------------------------------------
// ゲームメイン処理（メインループからコールされる）
//---------------------------------------------------------------------------------------
void CGameMain::Update()
{
	// ----- マウス更新
	m_Mouse.Update();
	// ----- シーン更新
	m_pScene->Update();
}

//---------------------------------------------------------------------------------------
// 描画処理（CGraphics::Render() からコールされる）
//---------------------------------------------------------------------------------------
void CGameMain::Draw()
{
#ifdef __DEBUG
	m_szDebug[0] = _T('\0');	// デバッグ文字列初期化

	// FPS を画面に描画するための文字列を作成
	TCHAR	str[256];
	_stprintf(str, _T("FPS = %d\n"), m_FPS);
	lstrcat(m_szDebug, str);
#endif
	
	// ----- シーン描画
	m_pScene->Draw();

	// デバッグ文字列描画
	m_pGraph->DrawText(0, 0, m_szDebug);
}

// デバッグ用文字列セット
void CGameMain::SetDebugText(TCHAR* str)
{
	lstrcat(m_szDebug, str);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 状態切り替え
//	Description : 現在のゲームステートを任意に切り替える
//	Arguments   : id / シーンID
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameMain::SetScene(int id)
{
	// ----- 次のシーンへ
	if(m_pScene)
	{
		m_pScene->Uninit();
	}
	if(id >= 0 && id < MAX_SCENEID)
	{
		m_pScene = m_sceneList[id];
		m_pScene->Init();
	}
	else
	{
		m_pScene = NULL;
		::MessageBox(NULL, _T("CGameMain::SetScene シーンIDが規定外です。"), _T("error"), MB_OK);
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : BGM再生
//	Description : 任意のBGMを再生する
//	Arguments   : id       / BGMのサウンドID
//				  loop     / ループ再生フラグ(true:ループ有)
//				  priority / 優先度
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameMain::PlayBGM(int id, int loop, int priority)
{
	m_pBGM[id]->Play(0, priority, loop);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : SE再生
//	Description : 任意のSEを再生する
//	Arguments   : id       / SEのサウンドID
//				  loop     / ループ再生フラグ(true:ループ有)
//				  priority / 優先度
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameMain::PlaySE(int id, int loop, int priority)
{
	m_pSE[id]->Play(0, priority, loop);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : BGM停止
//	Description : 任意のBGMを停止する
//	Arguments   : id / BGMのサウンドID
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameMain::StopBGM(int id)
{
	m_pBGM[id]->Stop();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : SE停止
//	Description : 任意のSEを停止する
//	Arguments   : id / SEのサウンドID
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameMain::StopSE(int id)
{
	m_pSE[id]->Stop();
}


// End of File