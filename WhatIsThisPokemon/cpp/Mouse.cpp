//================================================================
//		File        : Mouse.cpp
//		Program     : 背景オブジェクトベース
//
//		Description : 背景オブジェクトベースの実装
//
//		History     : 2014/07/08	作成開始
//
//										Author : Masanori Shibata
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Mouse.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------

//================================================================
// public:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CMouse::CMouse()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CMouse::~CMouse()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : オブジェクトデータの後始末をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CMouse::Uninit(void)
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新
//	Description : オブジェクトを更新する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CMouse::Update(void)
{
	// ===== マウス座標更新
	GetCursorPos(&m_MousePos);
	ScreenToClient(HWND(), &m_MousePos);

	// ===== 入力情報更新
	// ----- 左クリック
	if(GetAsyncKeyState(MK_LBUTTON) & 0x8000)
		m_Input[LBUTTON].m_Read	= true;
	else
		m_Input[LBUTTON].m_Read	= false;

	// ----- 右クリック
	if(GetAsyncKeyState(MK_RBUTTON) & 0x8000)
		m_Input[RBUTTON].m_Read	= true;
	else
		m_Input[RBUTTON].m_Read	= false;

	// ----- マウス情報更新
	for(int i = 0; i < MAX_BUTTON; i++)
	{
		m_Input[i].m_Trg = (m_Input[i].m_Prs ^ m_Input[i].m_Read) & m_Input[i].m_Read;
		m_Input[i].m_Rls = (m_Input[i].m_Prs ^ m_Input[i].m_Read) & m_Input[i].m_Prs;
		m_Input[i].m_Prs = m_Input[i].m_Read;
	}

}

// End of File