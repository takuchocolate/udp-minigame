//================================================================
//		File        : GameOver.cpp
//		Program     : ゲームオーバー画面操作
//
//		Description : ゲームオーバー画面に関する更新や描画を行い、操作する
//
//		History     : 2013/12/18	作成開始
//
//										Author : Kei Hashimoto
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/GameOver.h"
#include "../h/GameMain.h"
#include "../h/Input.h"
#include "../h/System.h"

using namespace Input;
using namespace System;

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
// ----- リソース情報
const LPCTSTR TEXTURE_FILENAME[MAX_TEX_GAMEOVER] =	{	_T("resource/texture/title/BG.jpg"),		// 背景テクスチャファイル名
													};

// ----- テクスチャ頂点座標
const D3DXVECTOR4	INIT_VTX2D_VTX[MAX_TEX_GAMEOVER][4]	=	{{	// 背景
																DEFAULT_VTX2D_VTX[0],
																DEFAULT_VTX2D_VTX[1],
																DEFAULT_VTX2D_VTX[2],
																DEFAULT_VTX2D_VTX[3]},
															 {	// タイトルへメニュー
															 	D3DXVECTOR4(335.0f, 400.0f, 0.0f, 1.0f),
																D3DXVECTOR4(485.0f, 400.0f, 0.0f, 1.0f),
																D3DXVECTOR4(335.0f, 474.0f, 0.0f, 1.0f),
																D3DXVECTOR4(485.0f, 474.0f, 0.0f, 1.0f)},
															 {	// 終了メニュー
															 	D3DXVECTOR4(335.0f, 475.0f, 0.0f, 1.0f),
																D3DXVECTOR4(485.0f, 475.0f, 0.0f, 1.0f),
																D3DXVECTOR4(335.0f, 549.0f, 0.0f, 1.0f),
																D3DXVECTOR4(485.0f, 549.0f, 0.0f, 1.0f)},
															};


//================================================================
// public:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CGameOver::CGameOver()
{
	m_pCamera = NULL;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CGameOver::~CGameOver()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : ゲームオーバー画面を初期化する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameOver::Init(void)
{
	// ----- カメラ初期化
	m_pCamera->Init();
	D3DXVECTOR3 eye = D3DXVECTOR3(0, 0, 100);
	D3DXVECTOR3 look = D3DXVECTOR3(0, 0, -100);
	D3DXVECTOR3 up = D3DXVECTOR3(0, 1, 0);
	m_pCamera->SetParameter(eye, look, up);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : ゲームオーバー画面の終了処理をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameOver::Uninit(void)
{
	m_pCamera->Uninit();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新
//	Description : ゲームオーバー画面を更新する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameOver::Update(void)
{
	m_pCamera->Update();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 描画
//	Description : ゲームオーバー画面を描画する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameOver::Draw(void)
{
	m_pCamera->Draw();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 生成処理
//	Description : ゲームオーバー画面データを生成する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CGameOver* CGameOver::Create()
{
	// ----- 変数宣言
	CGameOver* pGameOver;

	// ----- 初期化処理
	pGameOver = new CGameOver();
	if(pGameOver)
	{
		if(!pGameOver->Initialize())
		{
			SAFE_DELETE(pGameOver);
		}
	}

	return pGameOver;
}


//================================================================
// private:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : ゲームオーバー画面を初期化する
//	Arguments   : None.
//	Returns     : 成否(true:成功)
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CGameOver::Initialize()
{
	// カメラ生成
	m_pCamera = CCamera::Create();
	if(m_pCamera == NULL)
	{
		::MessageBox(NULL, _T("CGameOver::Cameraの生成に失敗しました。"), _T("error"), MB_OK);
		return false;
	}
	
	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : ゲームオーバー画面の終了処理をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGameOver::Finalize(void)
{
	// ----- オブジェクト解放
	SAFE_RELEASE(m_pCamera);		// カメラデータ
}


// End of File