//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/Title.h"
#include "../h/BackgroundManager.h"

//----------------------------------------------------------------
// グローバル変数宣言
//----------------------------------------------------------------
//----------------------------------------------------------------
// 処理関数
//----------------------------------------------------------------
// デフォルト値で初期化
void CBackgroundManager::Init()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pBG[i]->Init();
		m_pBG[i]->Translation(D3DXVECTOR3(SCREEN_WIDTH*i, 0.0f, 0.0f));
	}
}
// 後始末
void CBackgroundManager::Uninit()
{}
// 更新
void CBackgroundManager::Update()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pBG[i]->Update();
	}

	if(m_pBtnMng->getButton()[PIKEMON_BUTTON]->getPush())
	{
		m_eState	= BG_MOVE_FIELD_TO_POKEMONTABLE;
	}
	else if(GetAsyncKeyState('9') & 0x8000)
	{
		m_eState	= BG_MOVE_POKEMONTABLE_TO_FIELD;
	}
	else
	{
		m_vPos.x	= 0.0f;
	}
	// ===== 背景移動
	// ----- フィールドからテーブルへ
	if(m_eState == BG_MOVE_FIELD_TO_POKEMONTABLE)
	{
		if(m_pBG[TITLE_POKEMONTABLE]->GetMatrix()._41 > SCREEN_WIDTH/2.0f)
		{
			m_vPos.x = -10.0f;
		}
		else
		{
			m_vPos.x = SCREEN_WIDTH/2.0f - m_pBG[TITLE_POKEMONTABLE]->GetMatrix()._41;
			m_eState	= BG_POKEMONTABLE;
		}
	}
	// ----- テーブルからフィールドへ
	if(m_eState == BG_MOVE_POKEMONTABLE_TO_FIELD)
	{
		if(m_pBG[TITLE_FIELD]->GetMatrix()._41 < SCREEN_WIDTH/2.0f)
		{
			m_vPos.x = +10.0f;
		}
		else
		{
			m_vPos.x = SCREEN_WIDTH/2.0f - m_pBG[TITLE_FIELD]->GetMatrix()._41;
			m_eState	= BG_FIELD;
		}
	}

	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pBG[i]->Translation(D3DXVECTOR3(	m_vPos.x,
											m_vPos.y,
											m_vPos.z));
	}
}
// 描画
void CBackgroundManager::Draw()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pBG[i]->Draw();
	}
}
// 透明オブジェ描画
void CBackgroundManager::DrawAlpha()
{
	// ----- 背景
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pBG[i]->DrawAlpha();
	}
}
// 生成
CBackgroundManager* CBackgroundManager::Create()
{
	// ----- 変数宣言
	CBackgroundManager* pBackgroundManager;

	// ----- 初期化処理
	pBackgroundManager = new CBackgroundManager();

	pBackgroundManager->setCnt(DEFAULT_MAX_BG);

	if(pBackgroundManager)
	{
		if(!pBackgroundManager->Initialize())
		{
			SAFE_DELETE(pBackgroundManager);
		}
	}

	return pBackgroundManager;
}
void CBackgroundManager::Release()
{
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pBG[i]->Release();
	}
}
// ----- 背景マネージャ(個数, ファイル名)
CBackgroundManager* CBackgroundManager::Create(int nCnt, LPCTSTR* FileName)
{
	// ----- 変数宣言
	CBackgroundManager* pBackgroundManager;
	// ----- 初期化処理
	pBackgroundManager = new CBackgroundManager();

	if(pBackgroundManager)
	{
		if(!pBackgroundManager->Initialize(nCnt, FileName))
		{
			SAFE_DELETE(pBackgroundManager);
		}
	}
	return pBackgroundManager;
}
// コンストラクタ
CBackgroundManager::CBackgroundManager()
{
	m_pBG		= NULL;
	m_FileName	= NULL;
	m_nCnt		= 0;
	m_vPos		= D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_eState	= BG_WAIT;
}
// デストラクタ
CBackgroundManager::~CBackgroundManager()
{}
// 初期化
bool CBackgroundManager::Initialize()
{
	// ------ 背景作成
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pBG[i] = CBackground::Create(m_FileName[i]);
	}

	return true;
}
// 初期化
bool CBackgroundManager::Initialize(int nCnt, LPCTSTR* FileName)
{
	// 数を確保
	this->setCnt(nCnt);

	// BG作成
	if(m_pBG != NULL)
		delete[] m_pBG;
	m_pBG	= new CBackground*[nCnt];

	// ファイル作成
	if(m_FileName != NULL)
		delete m_FileName;
	m_FileName = new LPCTSTR[sizeof(FileName)/sizeof(LPCTSTR)];
	this->setFileName(FileName);

	// ------ 背景作成
	for(int i = 0; i < m_nCnt; ++i)
	{
		m_pBG[i] = CBackground::Create(m_FileName[i]);
	}

	return true;
}
// 後始末
void CBackgroundManager::Finalize()
{
	for(int i = 0; i < m_nCnt; ++i)
	{
		SAFE_RELEASE(m_pBG[i]);
	}
}
