//================================================================
//		File        : System.cpp
//		Program     : ゲーム共通システム管理
//
//		Description : ゲームの共通システムを管理する
//
//		History     : 2013/12/05	作成開始
//					  2014/01/14	フェードイン、フェードアウト関数作成
//
//										Author : Kei Hashimoto
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/System.h"

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
const float	SCREEN_WIDTH_3D		= 20.0f;		// スクリーン境界判定用サイズ(横幅)
const float	SCREEN_HEIGHT_3D	= 20.0f;		// スクリーン境界判定用サイズ(縦幅)


//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : スクリーン境界判定
//	Description : 任意の対象とスクリーンとの境界判定を行う
//	Arguments   : 対象座標(D3DXVECTOR3型参照)
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void System::IntersectScreen(D3DXVECTOR3& Pos)
{
	if(Pos.x < -SCREEN_WIDTH_3D / 2)			// 画面左端
	{
		Pos.x = -SCREEN_WIDTH_3D / 2;	
	}
	if(Pos.x > SCREEN_WIDTH_3D / 2)			// 画面右端
	{
		Pos.x = SCREEN_WIDTH_3D / 2;
	}
	if(Pos.y > SCREEN_HEIGHT_3D / 2)			// 画面上端
	{
		Pos.y = SCREEN_HEIGHT_3D / 2;
	}
	if(Pos.y < -SCREEN_HEIGHT_3D / 2)		// 画面下端
	{
		Pos.y = -SCREEN_HEIGHT_3D / 2;
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 境界球の当たり判定
//	Description : 任意の対象同士で境界球の境界判定を行う
//	Arguments   : 1つ目の境界球中心座標(D3DXVECTOR3型参照),
//				  1つ目の境界球半径(float型),
//				  1つ目のワールド変換行列(D3DXMATRIX型参照),
//				  2つ目の境界球中心座標(D3DXVECTOR3型参照),
//				  2つ目の境界球半径(float型),
//				  2つ目のワールド変換行列(D3DXMATRIX型参照)
//	Returns     : 判定結果(bool型)[true:当たっている]
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool System::IntersectSphere(
	const D3DXVECTOR3&	vCenter1,		// 1つ目の境界球中心座標
	const float			fRadius1,		// 1つ目の境界球半径1
	const D3DXMATRIX&	Matrix1,		// 1つ目のワールド変換行列
	const D3DXVECTOR3&	vCenter2,		// 2つ目の境界球中心座標2
	const float			fRadius2,		// 2つ目の境界球半径2
	const D3DXMATRIX&	Matrix2)		// 2つ目のワールド変換行列2
{
	// 中心座標のワールド座標の計算
	D3DXVECTOR3 vCenterTemp1, vCenterTemp2;
	D3DXVec3TransformCoord(&vCenterTemp1, &vCenter1, &Matrix1);
	D3DXVec3TransformCoord(&vCenterTemp2, &vCenter2, &Matrix2);

	// ワールド座標上の中心座標間の距離(の二乗)
	float fDist = D3DXVec3LengthSq(&(vCenterTemp2 - vCenterTemp1));
	// 半径の合計(の二乗)
	float fRadi = (fRadius1 + fRadius2) * (fRadius1 + fRadius2);
	
	//　半径の合計より距離が短ければ当たっている
	return (fDist < fRadi);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : フェードイン
//	Description : 対象を任意の間隔でフェードインする
//	Arguments   : 対象(VERTEX2D型ポインタ), フェード間隔(int型)
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void System::FadeInColor(VERTEX2D* pOut, int nFade)
{
	// ----- 変数宣言
	DWORD dwTempRed;		// r値格納バッファ
	DWORD dwTempGreen;		// g値格納バッファ
	DWORD dwTempBlue;		// b値格納バッファ

	// ----- 初期化処理
	dwTempRed	= 0;	
	dwTempGreen	= 0;	
	dwTempBlue	= 0;

	// ----- エラー処理
	if(nFade < 0)
		nFade = 0;
	if(nFade > 255)
		nFade = 255;
	
	// ----- フェードイン
	for(int nCntVtx = 0; nCntVtx < 4; nCntVtx ++)
	{
		dwTempRed	= ((pOut+ nCntVtx)->col & 0x00ff0000);					// r値抽出
		dwTempGreen	= ((pOut+ nCntVtx)->col & 0x0000ff00);					// g値抽出
		dwTempBlue	= ((pOut+ nCntVtx)->col & 0x000000ff);					// b値抽出
		(pOut+ nCntVtx)->col ^= (dwTempRed | dwTempGreen | dwTempBlue);		// アルファ値クリア
		dwTempRed	>>= 16;													// 整数へ変換
		dwTempGreen	>>= 8;													// 整数へ変換
		if(dwTempRed < 255)													// フェード更新
			dwTempRed += nFade;
		if(dwTempGreen < 255)												// フェード更新
			dwTempGreen += nFade;
		if(dwTempBlue < 255)												// フェード更新
			dwTempBlue += nFade;
		if(dwTempRed > 255)													// エラー処理
			dwTempRed = 255;
		if(dwTempGreen > 255)												// エラー処理
			dwTempGreen = 255;
		if(dwTempBlue > 255)												// エラー処理
			dwTempBlue = 255;
		dwTempRed	<<= 16;													// r位置へ戻す
		dwTempGreen	<<= 8;													// g位置へ戻す
		(pOut+ nCntVtx)->col |= (dwTempRed | dwTempGreen | dwTempBlue);		// フェード反映
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : フェードアウト
//	Description : 対象を任意の間隔でフェードアウトする
//	Arguments   : 対象(VERTEX2D型ポインタ), フェード間隔(int型)
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void System::FadeOutColor(VERTEX2D* pOut, int nFade)
{
	// ----- 変数宣言
	DWORD dwTempRed;		// r値格納バッファ
	DWORD dwTempGreen;		// g値格納バッファ
	DWORD dwTempBlue;		// b値格納バッファ

	// ----- 初期化処理
	dwTempRed	= 0;	
	dwTempGreen	= 0;	
	dwTempBlue	= 0;

	// ----- エラー処理
	if(nFade < 0)
		nFade = 0;
	if(nFade > 255)
		nFade = 255;
	
	// ----- フェードアウト
	for(int nCntVtx = 0; nCntVtx < 4; nCntVtx ++)
	{
		dwTempRed	= ((pOut+ nCntVtx)->col & 0x00ff0000);					// r値抽出
		dwTempGreen	= ((pOut+ nCntVtx)->col & 0x0000ff00);					// g値抽出
		dwTempBlue	= ((pOut+ nCntVtx)->col & 0x000000ff);					// b値抽出
		(pOut+ nCntVtx)->col ^= (dwTempRed | dwTempGreen | dwTempBlue);		// アルファ値クリア
		dwTempRed	>>= 16;													// 整数へ変換
		dwTempGreen	>>= 8;													// 整数へ変換
		if(dwTempRed > 0)													// フェード更新
			dwTempRed -= nFade;
		if(dwTempGreen > 0)													// フェード更新
			dwTempGreen -= nFade;
		if(dwTempBlue > 0)													// フェード更新
			dwTempBlue -= nFade;
		if(dwTempRed < 0)													// エラー処理
			dwTempRed = 0;
		if(dwTempGreen < 0)													// エラー処理
			dwTempGreen = 0;
		if(dwTempBlue < 0)													// エラー処理
			dwTempBlue = 0;
		dwTempRed	<<= 16;													// r位置へ戻す
		dwTempGreen	<<= 8;													// g位置へ戻す
		(pOut+ nCntVtx)->col |= (dwTempRed | dwTempGreen | dwTempBlue);		// フェード反映
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : フラッシュ
//	Description : 対象を任意の間隔でフラッシュする
//	Arguments   : 対象(VERTEX2D型ポインタ), フラッシュ間隔(int型)
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void System::FlashColor(VERTEX2D* pOut, int nFlash)
{
	FadeInColor(pOut, nFlash);
	FadeOutColor(pOut, nFlash);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 透過フェードイン
//	Description : 対象を任意の間隔で透過的にフェードインする
//	Arguments   : 対象(VERTEX2D型ポインタ), フェード間隔(int型)
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void System::FadeInAlpha(VERTEX2D* pOut, int nFade)
{
	// ----- 変数宣言
	DWORD dwTempAlpha;		// アルファ値格納バッファ

	// ----- 初期化処理
	dwTempAlpha = 0;

	// ----- エラー処理
	if(nFade < 0)
		nFade = 0;
	if(nFade > 255)
		nFade = 255;
	
	// ----- フェードイン
	for(int nCntVtx = 0; nCntVtx < 4; nCntVtx ++)
	{
		dwTempAlpha = ((pOut+ nCntVtx)->col & 0xff000000);		// アルファ値抽出
		(pOut+ nCntVtx)->col ^= dwTempAlpha;					// アルファ値クリア
		dwTempAlpha >>= 24;										// 整数へ変換
		if(dwTempAlpha < 255)									// フェード更新
			dwTempAlpha += nFade;
		if(dwTempAlpha > 255)									// エラー処理
			dwTempAlpha = 255;
		dwTempAlpha <<= 24;										// アルファ位置へ戻す
		(pOut+ nCntVtx)->col |= dwTempAlpha;					// フェード反映
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 透過フェードアウト
//	Description : 対象を任意の間隔で透過的にフェードアウトする
//	Arguments   : 対象(VERTEX2D型ポインタ), フェード間隔(int型)
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void System::FadeOutAlpha(VERTEX2D* pOut, int nFade)
{
	// ----- 変数宣言
	DWORD dwTempAlpha;		// アルファ値格納バッファ

	// ----- 初期化処理
	dwTempAlpha = 0;

	// ----- エラー処理
	if(nFade < 0)
		nFade = 0;
	if(nFade > 255)
		nFade = 255;
	
	// ----- フェードイン
	for(int nCntVtx = 0; nCntVtx < 4; nCntVtx ++)
	{
		dwTempAlpha = ((pOut+ nCntVtx)->col & 0xff000000);		// アルファ値抽出
		(pOut+ nCntVtx)->col ^= dwTempAlpha;					// アルファ値クリア
		dwTempAlpha >>= 24;										// 整数へ変換
		if(dwTempAlpha > 0)										// フェード更新
			dwTempAlpha -= nFade;
		if(dwTempAlpha < 0)										// エラー処理
			dwTempAlpha = 0;
		dwTempAlpha <<= 24;										// アルファ位置へ戻す
		(pOut+ nCntVtx)->col |= dwTempAlpha;					// フェード反映
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 透過フラッシュ
//	Description : 対象を任意の間隔で透過的にフラッシュする
//	Arguments   : 対象(VERTEX2D型ポインタ), フラッシュ間隔(int型)
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void System::FlashAlpha(VERTEX2D* pOut, int nFlash)
{
	FadeInAlpha(pOut, nFlash);
	FadeOutAlpha(pOut, nFlash);
}


// End of File