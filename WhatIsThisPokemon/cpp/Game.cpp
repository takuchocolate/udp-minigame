//================================================================
//		File        : Game.cpp
//		Program     : ゲーム本編操作
//
//		Description : ゲーム本編に関する更新や描画を行い、操作する
//
//		History     : 2013/12/18	作成開始
//
//										Author : Kei Hashimoto
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include "../h/MyWinSock.h"
#include "../h/Game.h"
#include "../h/Input.h"
#include "../h/System.h"
#include "../h/GameMain.h"

using namespace Input;
using namespace System;

#include <stdio.h>
#ifdef _DEBUG
bool CallConsole()
{
	if(AllocConsole() == 0)
	{
		::MessageBox(NULL, _T("CWinSock::コンソールの生成に失敗しました。"), _T("error"), MB_OK);
		return false;
	}
	freopen("CONOUT$", "w", stdout);
	freopen("CONIN$", "r", stdin);
	return true;
}
void DeleteConsole()
{
	if(FreeConsole() == 0)
	{
		::MessageBox(NULL, _T("CWinSock::コンソールの破棄に失敗しました。"), _T("error"), MB_OK);
	}
}
#endif

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------


//----------------------------------------------------------------
// メンバ実体宣言
//----------------------------------------------------------------
ePhase CGame::m_phase;	//フェーズ

//================================================================
// public:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CGame::CGame()
{
	m_pCamera = NULL;
	CallConsole();

	//デバッグ用
	CPokemon::LoadPokemonList();	//ポケモン一覧表読み込み
	CPokemon::LoadTypeTable();		//タイプ相性表読み込み
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CGame::~CGame()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : ゲーム本編を初期化する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGame::Init(void)
{
	// ----- カメラ初期化
	m_pCamera->Init();
	D3DXVECTOR3 eye = D3DXVECTOR3(0, 0, 100);
	D3DXVECTOR3 look = D3DXVECTOR3(0, 0, -100);
	D3DXVECTOR3 up = D3DXVECTOR3(0, 1, 0);
	m_pCamera->SetParameter(eye, look, up);

	CWinSock& sock = CWinSock::GetInstance();
	MatchData* recvData;

	switch(sock.GetType())
	{
	//サーバー-------------------------------------------------
	case 0:
		m_pDealer = new CDealer();			//ディーラー生成
		for(int i = 0; i < 4; i++)
		{
			m_pPlayer[i] = new CPlayer();	//プレイヤー生成
		}
		m_phase = PHASE_MATCH;				//フェーズ初期化
		//マッチング用データ設定
		MatchData matchData;
		for(int i = 0; i < 4; i++)
		{
			matchData.num = i;
			matchData.phase = m_phase;
			matchData.type = UNI;
			sock.SetSendServerData(i, (WINSOCK_PROTOCOL*)&matchData, sizeof(matchData));
		}

		printf("[Server]Start\n");
		break;

	//クライアント-------------------------------------------------
	case 1:
		//この辺でサーバーから初期化用データ受信
		recvData = (MatchData *)(sock.GetRecvClntProtocol());
		//ディーラー情報
		m_pDealer = new CDealer();
		//プレイヤー番号決定
		m_nPlayerNum = recvData->num;
		printf("[Client%d]Start\n",m_nPlayerNum);
		break;
	}

}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : ゲーム本編の終了処理をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGame::Uninit(void)
{
	m_pCamera->Uninit();
	DeleteConsole();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新
//	Description : ゲーム本編を更新する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGame::Update(void)
{
	CWinSock& sock = CWinSock::GetInstance();	//ソケット

	//サーバー/クライアント切替
	switch(sock.GetType())
	{
	case 0:
		Server();
		break;
	case 1:
		Client();
		break;
	}


}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 描画
//	Description : ゲーム本編を描画する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGame::Draw(void)
{	
	// ----- カメラ描画
	m_pCamera->Draw();

	if(m_pDealer)
//		m_pDealer->Draw();
	for(int i = 0; i < 4; i++)
	{
		if(!m_pPlayer[i])
			continue;

		m_pPlayer[i]->Draw();
	}
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 生成処理
//	Description : ゲーム本編データを生成する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CGame* CGame::Create()
{
	// ----- 変数宣言
	CGame* pGame;

	// ----- 初期化処理
	pGame = new CGame();
	if(pGame)
	{
		if(!pGame->Initialize())
		{
			SAFE_DELETE(pGame);
		}
	}

	return pGame;
}


//================================================================
// private:
//================================================================

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : ゲーム本編を初期化する
//	Arguments   : None.
//	Returns     : 成否(true;成功)
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CGame::Initialize()
{
	// カメラ生成
	m_pCamera = CCamera::Create();
	if(m_pCamera == NULL)
	{
		::MessageBox(NULL, _T("CGame::Cameraの生成に失敗しました。"), _T("error"), MB_OK);
		return false;
	}

	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : ゲーム本編の終了処理をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CGame::Finalize(void)
{
	// ----- オブジェクト解放
	SAFE_RELEASE(m_pCamera);		// カメラデータ
}

//------------------------------------------------------------
// @要　約：サーバーモード
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/07/16	近藤拓	新規作成
//------------------------------------------------------------
void CGame::Server()
{
	CWinSock& sock = CWinSock::GetInstance();	//ソケット
	GameData gameData;

	m_pDealer->Update();

	switch(m_phase)
	{
	//マッチング
	case PHASE_MATCH:
		//全員に配ったら次のフェーズへ
		if(sock.GetConnectNum() == 4)
		{
			m_phase = PHASE_START;
		}
		break;
	//ゲーム開始
	case PHASE_START:
		//なんか始まった演出
		//お題引く
		//カード配る
		break;
	//ポケモン提示
	case PHASE_PUT:
		for(int i = 0; i < 4; i++)
		{

		}
		break;
	//提示待ち
	case PHASE_WAIT_PUT:
		//他の人を待っていますと表示
		break;
	//相性判定
	case PHASE_COMP:
		//ディーラーが出したポケモンとの相性を発表
		break;
	//解答開始
	case PHASE_ANSWER:
		//早押しで解答できる
		break;
	//解答待ち
	case PHASE_WAIT_ANS:
		//他の人が解答中と表示
		break;
	//正誤判定
	case PHASE_JUDGE:
		printf("JUDGE\n");
		//正解ならPHASE_OPENへ、不正解ならPHASE_ANSWERへ
		bool result;

		//プレイヤーからの解答
		char buf[256];	//答えた名前
		scanf("%s",buf);
	
		result = m_pDealer->Judge(buf);
		m_phase = (result) ? PHASE_OPEN : PHASE_ANSWER;
		break;
	//答え公開
	case PHASE_OPEN:
		printf("OPEN\n");
		//公開
		//正解したプレイヤーへ渡すor破棄
		//PHASE_PUTへ戻る
		break;
	}

	//送信用データ設定
	gameData.dealer	= *m_pDealer;
	for(int i = 0; i < 4; i++)
	{
		gameData.player[i] = *m_pPlayer[i];
	}
	gameData.phase	= m_phase;
	gameData.type	= BROAD;

}
//------------------------------------------------------------
// @要　約：クライアントモード
// @引　数：なし
// @戻り値：なし
// @ノート：なし
// @更新日時2014/07/16	近藤拓	新規作成
//------------------------------------------------------------
void CGame::Client()
{
	//変数宣言
	CWinSock& sock = CWinSock::GetInstance();	//ソケット
	GameData gameData;							//受信データ
	gameData = *(GameData*)sock.GetRecvClntProtocol();
	m_pPlayer[m_nPlayerNum]->Update();

	//自分自身の情報
	CPlayer* pMyself = m_pPlayer[m_nPlayerNum];

	switch(m_phase)
	{
	//ゲーム開始
	case PHASE_START:
		//なんか始まった演出
		//マッチング中
		break;

	//ポケモン提示
	case PHASE_PUT:
		printf("PUT\n");
		pMyself->put();		//手札から出すポケモンを選択
		m_pDealer->SetAttacker(pMyself->getAttacker(), m_nPlayerNum);
		break;

	//提示待ち
	case PHASE_WAIT_PUT:
		printf("WAIT_PUT\n");
		//他の人を待っていますと表示
		break;

		//相性判定
	case PHASE_COMP:
		printf("COMP\n");
		//ディーラーが出したポケモンとの相性を発表
		break;

	//解答開始
	case PHASE_ANSWER:
		printf("ANSWER\n");
		//早押しで解答できる
		break;

	//解答待ち
	case PHASE_WAIT_ANS:
		printf("WAIT_ANS\n");
		//他の人が解答中と表示
		break;

	//正誤判定
	case PHASE_JUDGE:
		printf("JUDGE\n");
		break;

	//答え公開
	case PHASE_OPEN:
		printf("OPEN\n");
		//公開
		//正解したプレイヤーへ渡すor破棄
		//PHASE_PUTへ戻る
		break;
	}
	
	//データ送信
	sock.SetSendClientData((WINSOCK_PROTOCOL*)&gameData, sizeof(gameData));
}


// End of File