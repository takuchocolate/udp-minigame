//================================================================
//		File        : Title.cpp
//		Program     : タイトル画面操作
//
//		Description : タイトル画面の更新や描画を行い、操作する
//
//		History     : 2013/12/18	作成開始
//
//										Author : Kei Hashimoto
//================================================================


//----------------------------------------------------------------
// インクルード
//----------------------------------------------------------------
#include <tchar.h>
#include <stdio.h>
#include "../h/Title.h"
#include "../h/Input.h"
#include "../h/System.h"
#include "../h/GameMain.h"

using namespace Input;
using namespace System;

//----------------------------------------------------------------
// 定数定義
//----------------------------------------------------------------
// ===== 2Dオブジェクト情報
// ----- リソース情報
LPCTSTR CARD_FILENAME[MAX_CARDTITLE] =		{
												_T("res/tex/card/pokemon_texture_01.png"),	// カードテクスチャファイル名
											};
LPCTSTR BG_FILENAME[MAX_BGTITLE] =			{
												_T("res/tex/field/pokemonfield.png"),		// 背景テクスチャファイル名
												_T("res/tex/field/pokemontable.png"),		// 背景テクスチャファイル名
											};
LPCTSTR BUTTON_FILENAME[MAX_BUTTONTITLE] =	{
												_T("res/tex/button/menubtn01.png"),			// ボタンテクスチャファイル名
												_T("res/tex/button/menubtn02.png"),			// ボタンテクスチャファイル名
												//_T("res/tex/button/menubtn01.png"),			// ボタンテクスチャファイル名
											};
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : コンストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CTitle::CTitle()
{
	// カメラ初期設定
	m_pCamera = NULL;
	// ===== オブジェクト初期設定
	// ----- カード
	m_pCardMng	= NULL;
	// ----- 背景
	m_pBgMng	= NULL;
	// ----- ボタン
	m_pBtnMng	= NULL;

	m_PC	= NULL;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : デストラクタ
//	Arguments   : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CTitle::~CTitle()
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : タイトル画面を初期化する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTitle::Init(void)
{
	m_pCamera->Init();

	// ===== オブジェクト初期化
	// ----- カード
	m_pCardMng->Init();
	// ----- 背景
	m_pBgMng->Init();
	m_pBgMng->setBtnMng(m_pBtnMng);
	// ----- ボタン
	m_pBtnMng->Init();
	//m_pBtnMng->setBgMng(m_pBgMng);

	m_PC->Init(100.0f, 100.0f);

	D3DXVECTOR3 eye		= D3DXVECTOR3(SCREEN_WIDTH/2.0f, SCREEN_HEIGHT/2.0f, 100);
	D3DXVECTOR3 look	= D3DXVECTOR3(SCREEN_WIDTH/2.0f, SCREEN_HEIGHT/2.0f, -100);
	D3DXVECTOR3 up		= D3DXVECTOR3(0, 1, 0);
	m_pCamera->SetParameter(eye, look, up);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : タイトル画面の終了処理をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTitle::Uninit(void)
{
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 更新
//	Description : タイトル画面を更新する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTitle::Update(void)
{
	// カメラ更新
	m_pCamera->Update();

	// ===== オブジェクト更新
	// ----- カード
	m_pCardMng->Update();
	// ----- 背景
	m_pBgMng->Update();
	// ----- ボタン
	m_pBtnMng->Update();

	m_PC->Update();

	if(GetTrgKey(DIK_RETURN))
		CGameMain::SetScene(SID_GAME);
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 描画
//	Description : タイトル画面を描画する
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTitle::Draw(void)
{
	// 変数宣言
	TCHAR	sz[256];

	// ----- カメラ描画
	m_pCamera->Draw();
	// ===== 非透明オブジェクト描画
	// ----- カード
	m_pCardMng->Draw();
	// ----- 背景
	m_pBgMng->Draw();
	// ----- ボタン
	m_pBtnMng->Draw();

	m_PC->Draw();

	// ===== 透明オブジェクトの描画
	// ----- カード
	m_pCardMng->DrawAlpha();
	// ----- 背景
	m_pBgMng->DrawAlpha();
	// ----- ボタン
	m_pBtnMng->DrawAlpha();
	
	m_PC->DrawAlpha();
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 生成処理
//	Description : タイトル画面データを生成する
//	Arguments   : None.
//	Returns     : タイトル画面データ
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
CTitle* CTitle::Create()
{
	// ----- 変数宣言
	CTitle* pTitle;

	// ----- 初期化処理
	pTitle = new CTitle();
	if(pTitle)
	{
		if(!pTitle->Initialize())
		{
			SAFE_DELETE(pTitle);
		}
	}

	return pTitle;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 初期化
//	Description : タイトル画面を初期化する
//	Arguments   : None.
//	Returns     : 成否
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
bool CTitle::Initialize()
{
	// ----- カメラ作成
	m_pCamera = CCamera::Create();
	// ------ カード作成
	m_pCardMng	= CCardManager::Create(MAX_CARDTITLE, CARD_FILENAME);
	// ------ 背景作成
	m_pBgMng	= CBackgroundManager::Create(MAX_BGTITLE, BG_FILENAME);
	// ------ ボタン作成
	m_pBtnMng	= CButtonManager::Create(MAX_BUTTONTITLE, BUTTON_FILENAME);

	m_PC		= CPokemonCard::Create("ロコン");

	return true;
}

//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
//	Name        : 後始末
//	Description : タイトル画面の終了処理をする
//	Arguments   : None.
//	Returns     : None.
//━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
void CTitle::Finalize(void)
{
	//SAFE_RELEASE(m_pObj);

	SAFE_RELEASE(m_PC);
	// ----- 背景破棄
	SAFE_RELEASE(m_pBtnMng);
	// ----- 背景破棄
	SAFE_RELEASE(m_pBgMng);
	// ----- カード破棄
	SAFE_RELEASE(m_pCardMng);
	// ----- カメラ破棄
	SAFE_RELEASE(m_pCamera);
}


// End of File