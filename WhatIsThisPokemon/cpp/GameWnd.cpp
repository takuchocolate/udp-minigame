//=======================================================================================
//
//	  ゲーム用ウィンドウ クラス定義
//
//=======================================================================================
#include "../h/GameWnd.h"
#include "../h/System.h"

//---------------------------------------------------------------------------------------
//	ウィンドウ初期化
//---------------------------------------------------------------------------------------
bool CGameWindow::InitInstance()
{
	if (!CWindow::InitInstance()) return false;

	m_bWindow = false;
	if (IDYES == MessageBox(_T("ウィンドウモードで実行しますか？"), _T("画面モード"), MB_YESNO))
		m_bWindow = true;

	::timeBeginPeriod(1);	// タイマの分解能を最小にセット

	// グラフィックデバイス初期化
	m_pGraph = CGraphics::Create(GetHwnd(), SCREEN_WIDTH, SCREEN_HEIGHT, m_bWindow);
	if (!m_pGraph) return false;
	
	// サウンドデバイス初期化
	m_pSound = new CSound;
	m_pSound->InitSound(GetHwnd());

	// 入出力デバイス初期化
	m_pInput = new CInput;
	m_pInput->InitInput(GetInstance(), GetHwnd());

	// シーン初期化
	m_pGameMain = CGameMain::Create(m_pGraph, m_pSound);
	if (!m_pGameMain) return false;

	m_dwFPSLastTime = ::timeGetTime();
	m_dwExecLastTime = m_dwFPSLastTime - (DWORD)FRAME_RATE;// 必ず最初に m_pGameMain->Update() を実行
	m_dwFrameCount = 0;

	return true;
}

//---------------------------------------------------------------------------------------
//	ウィンドウ解放
//---------------------------------------------------------------------------------------
int CGameWindow::ExitInstance()
{
	// シーン解放
	SAFE_RELEASE(m_pGameMain);

	// 入出力デバイス解放
	SAFE_RELEASE(m_pInput);

	// サウンドデバイス解放
	SAFE_RELEASE(m_pSound);

	// グラフィック デバイス解放
	SAFE_RELEASE(m_pGraph);

	::timeEndPeriod(1);				// タイマの分解能を元に戻す

	return CWindow::ExitInstance();	// 実行ファイルの戻り値
}

//---------------------------------------------------------------------------------------
//	アイドル時処理
//---------------------------------------------------------------------------------------
bool CGameWindow::OnIdle(long lCount)
{
	// この辺でフレーム数カウント
	DWORD dwCurrentTime = ::timeGetTime();			// 現在のタイマ値を取得
	if (dwCurrentTime - m_dwFPSLastTime >= 500) {	// 0.5 秒ごとに計測
		// フレーム数を計算
		if (m_pGameMain) {
			m_pGameMain->SetFPS(m_dwFrameCount * 1000 / (dwCurrentTime - m_dwFPSLastTime));
		}
		m_dwFPSLastTime = dwCurrentTime;	// タイマ値を更新
		m_dwFrameCount = 0;					// フレームカウンタをリセット
	}
	// この辺で時間管理
	while (dwCurrentTime - m_dwExecLastTime >= FRAME_RATE) {	// 一定時間が経過したら…
		m_dwExecLastTime += (DWORD)FRAME_RATE;					// タイマ値を更新
		if (m_pGameMain) {
			m_pInput->UpdateInput(GetHwnd());						// 入力デバイス情報更新
			m_pGameMain->Update();								// ゲーム メイン処理
		}
	}
	if (m_pGameMain) {
		m_pGameMain->Render();			// レンダリング処理
	}
	if(m_pGameMain->GetEndFlg())		// ゲーム終了
		OnClose();
	m_dwFrameCount++;					// フレームカウント＋１
	return true;
}

//---------------------------------------------------------------------------------------
//	WM_KEYDOWN ハンドラ
//---------------------------------------------------------------------------------------
void CGameWindow::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// [Esc] が押されていたら
	if (nChar == VK_ESCAPE) {
		// ウィンドウを閉じる
		OnClose();
	}
}

//---------------------------------------------------------------------------------------
//	WM_ERASEBKGND ハンドラ
//---------------------------------------------------------------------------------------
bool CGameWindow::OnEraseBkgnd(HDC hdc)
{
	// 背景消去しないで消去済みとして返す
	return true;
}

//---------------------------------------------------------------------------------------
//	WM_SETCURSOR ハンドラ
//---------------------------------------------------------------------------------------
bool CGameWindow::OnSetCursor(HWND hwnd, UINT nHitTest, UINT uMouseMsg)
{
	// 全画面モードか、マウスカーソル座標がクライアント領域内なら
	if (!m_bWindow || nHitTest == HTCLIENT) {
		// マウスカーソル消去
		//::SetCursor(NULL);
		return true;
	}
	return false;
}

//=======================================================================================
//	End of File
//=======================================================================================